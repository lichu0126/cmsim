/*************************************************************************
    > File Name: policy_cflru.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月29日 星期日 13时44分45秒
 ************************************************************************/

#include "cache_sim.h"

static LIST_HEAD(lru_list);
static int clean_first_window;



static struct cache_block * cache_replace(cache_manager *cm)
{
	struct list_head *lru;
	struct cache_block *cb = NULL;
	int i, find = 0;

	lru = lru_list.prev;

	for( i = 0; i < clean_first_window; i++){
		cb = list_entry(lru, struct cache_block, cb_list);

		if( !test_bit( CB_DIRTY, &cb->cb_flags )) {
			find = 1;
			break;
		}
		lru = lru->prev;
	}
	if (! find ) {
		lru = lru_list.prev;
		cb = list_entry (lru, struct cache_block, cb_list);
	}

	list_del_init(lru);
	cm_remove_hash(cb);

	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags)){
		/*
		cm->cm_dirty_count--;
		cm_disk_write(cm);
		*/
		cm_flush_block(cb);
	}
	else
		cm->cm_clean_count--;

	clear_bit(CB_RESIDENT, &cb->cb_flags);
	dprintf("replce lba=%d\n",cb->cb_blkno);
	return cb;

}

static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;		

	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		dprintf("cache hit lba=%d, op=%d\n",lba,op);
		list_move(&cb->cb_list, &lru_list);	/*move to MRU*/
		if ( op == WRITE ) {
			dprintf("cache hit lba=%d, op=%d dirty=%d\n",lba,op,test_bit(CB_DIRTY,&cb->cb_flags));
			if(test_bit(CB_DIRTY, &cb->cb_flags))        
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			cm->cm_write_hits++;
		}
		else {
			cm->cm_read_hits ++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}
	}

	/*cache miss*/
	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
		list_add(&cb->cb_list, &lru_list);
		if ( op == READ || fetch )
			cm_fetch_block(cb, lba);
/*		else
			cm_write_block(cb);
			*/  
		/* It's really a big and stupid mistake here!
		 * Fixed@2014-09-23*/
		if ( op == WRITE ) 
			cm_write_block(cb);
	}
	return 1;
}


static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}



struct cm_personality cflru_personality = 
{
	.policy_name = "cflru",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int cflru_register(cache_manager *cm) {

	clean_first_window = cm->cm_size/10;  /*10% of total cache size*/
	cm_personality_register(cm, &cflru_personality);
	return 0;
}
