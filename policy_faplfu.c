/*************************************************************************
    > File Name: faplfu.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月27日 星期五 21时51分18秒
	Flash-Aware Perfect LFU, try to keep pages with high write frequencies
 ************************************************************************/

#include "cache_sim.h"

#define REALLOC_STEP	32

#define READSKEW	0
#define WRITESKEW	(1-READSKEW)


typedef struct block_position {
	struct hlist_node hash;
//	struct cache_block *cb;
	int lba;
	int freq;	/*frequency*/
	int read_freq;
	int write_freq;
	int idx;	/*the end idx*/
	int len;	/*length of pos now*/
	int next_idx;
	int heap_idx;	/*idx in the heapmax*/
	int *pos;
}blk_pos;

#define MIN_HASH_NUM	( 1024*1024)
#define MIN_HASH_MASK	( MIN_HASH_NUM - 1)

static LIST_HEAD(faplfu_list);

static blk_pos * heapmax;
//static int heapsize=0;

static struct hlist_head *hashtbl;


static int max_pos_len = 0;
static int cache_bsize_sectors;

static void faplfu_insert_hash(struct block_position *bp)
{
	int hash = (bp->lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];
	hlist_add_head(&bp->hash, hp);
}

static blk_pos * faplfu_search_hash(int lba)
{
	struct block_position *bp;
	struct hlist_node *hn;
	int hash = (lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];

	//printf("find %d, hash=%d hlist blkno=%d\n",lba,hash,cb->cb_blkno);
	hlist_for_each_entry(bp, hn, hp, hash) {
		if ( bp->lba == lba)
			return bp;
	} 
	dprintf("lba %d not in cache\n", lba);
	return NULL;
}

static void  faplfu_update_frequency(struct cache_block * cb, int op)
{
	blk_pos *bp = NULL;

	if (( bp = faplfu_search_hash(cb->cb_blkno)) == NULL ) {
		bp = calloc (1, sizeof(blk_pos));
		bp->lba = cb->cb_blkno;
		INIT_HLIST_NODE(&bp->hash);
		faplfu_insert_hash(bp);
	}
	bp->freq++;
	if(op == READ )
		bp->read_freq++;
	else 
		bp->write_freq++;
	cb->cb_freq = (double)bp->read_freq * READSKEW + (double)bp->write_freq * WRITESKEW ;
}


void faplfu_scan_trace (cache_manager *cm, ioreq_event *new )
{
	int lba_start_align, lba_start, lba_end ;
	static int reqcount = 0 ;
	blk_pos *bp = NULL;
	
	lba_start = new->blkno;
	lba_start_align  = lba_start & ~(cm->cm_bsize/SECTOR_SIZE - 1);
	lba_end = new->blkno + new->bcount;

	dprintf("max_pos_len is %d\n",max_pos_len);
	for (;lba_start_align < lba_end; lba_start_align += cm->cm_bsize/SECTOR_SIZE) 
	{
		reqcount++;
		int lba = lba_start_align;
		if (( bp = faplfu_search_hash(lba)) == NULL ) {
			bp = calloc (1, sizeof(blk_pos));
			bp->lba = lba;
			bp->pos = calloc(REALLOC_STEP , sizeof(int));
			if(bp->pos == NULL) {
				printf("malloc error!\n");
				exit(-1);
			}
			bp->len = REALLOC_STEP;
			bp->pos[0] = reqcount;
			bp->idx	= 0;
			INIT_HLIST_NODE(&bp->hash);
			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		
			faplfu_insert_hash(bp);
		}
		else {
			if(bp->idx == (bp->len - 1)) {
				bp->pos = realloc(bp->pos, (bp->len + REALLOC_STEP) * sizeof(int));
				if(bp->pos == NULL) {
					printf("realloc error!\n");
					exit(-1);
				}
				bp->len += REALLOC_STEP;
			}
			bp->idx++;
			bp->pos[bp->idx] = reqcount;

			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		}
	}

}



static struct cache_block * cache_replace(cache_manager *cm)
{
	struct cache_block *cb, *min=NULL;
	int min_freq = (1<<30);

	/*find the cb with max nest_pos*/

	list_for_each_entry(cb, &faplfu_list, cb_list) {
		if ( cb->cb_freq == 0){
			min = cb;
			break;
		}
		else if (cb->cb_freq < min_freq){
			min_freq = cb->cb_freq;
			min = cb;
		}
	}

	cb = min;
	cm_remove_hash(cb);
	list_del_init(&cb->cb_list);
//	heap_delete(cb);
	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags))
		cm_flush_block (cb)	;
	else
		cm->cm_clean_count--;
	clear_bit(CB_RESIDENT, &cb->cb_flags);
//	printf("replace lba:%d\n",cb->cb_blkno);
	return cb;
}







static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;		
	dprintf("lba=%d, op=%d\n",lba,op);

	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		dprintf("cache hit lba=%d, op=%d\n",lba,op);
		if ( op == WRITE ) {
			if(test_bit(CB_DIRTY, &cb->cb_flags))        
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			cm->cm_write_hits++;
		}
		else {
			cm->cm_read_hits ++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}
	}
	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
//		heap_insert(cb);
		list_add(&cb->cb_list, &faplfu_list);

		if ( op == READ || fetch ) {
			cm_fetch_block(cb, lba);
		}
		if ( op == WRITE ) 
			cm_write_block(cb);
	}

	faplfu_update_frequency(cb, op);
	return 1;
}

static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}


struct cm_personality faplfu_personality = 
{
	.policy_name = "faplfu",
	.cache_read = cache_read,
	.cache_write = cache_write,
};



int faplfu_register(cache_manager *cm)
{
	cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;
	heapmax = calloc(cm->cm_size, sizeof(blk_pos *));
	hashtbl = (struct hlist_head *) calloc(MIN_HASH_NUM, sizeof(struct hlist_head));
	cm_personality_register(cm, &faplfu_personality);
	return 0;
}
