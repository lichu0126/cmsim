#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../sim

#./test.sh trace.list 
Policy=("faplfu")
PolicyCount=${#Policy[@]}
WSS=1024
LINES=131072

	CacheSize=$CacheSizeMin
	while [ $CacheSize -le $CacheSizeMax ]
	do
		for((i=0; i<PolicyCount; i++))
		do
			echo "Policy=${Policy[i]}, CacheSize=$CacheSize"
			log=zipf_${Policy[i]}_$WSS"_"$LINES.log
			echo $log
			$PROGRAM -s $CacheSize -i90000000 -p ${Policy[i]} -t zipf -w $WSS -l $LINES -a 0.86 >>	$log
		done

		((CacheSize *= 2))
	done

