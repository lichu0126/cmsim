/*************************************************************************
    > File Name: mins.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月27日 星期五 21时51分18秒
	MIN with extention for selective caching
	references to "SieveStore"
 ************************************************************************/

#include "cache_sim.h"

#define REALLOC_STEP	32



typedef struct block_position {
	struct hlist_node hash;
//	struct cache_block *cb;
	int lba;
	int idx;	/*the end idx*/
	int len;	/*length of pos now*/
	int next_idx;
	int heap_idx;	/*idx in the heapmax*/
	int *pos;
}blk_pos;

#define MIN_HASH_NUM	( 1024*1024)
#define MIN_HASH_MASK	( MIN_HASH_NUM - 1)

static LIST_HEAD(mins_list);

static blk_pos * heapmax;
//static int heapsize=0;

static struct hlist_head *hashtbl;


static int max_pos_len = 0;
static int cache_bsize_sectors;

static void mins_insert_hash(struct block_position *bp)
{
	int hash = (bp->lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];
	hlist_add_head(&bp->hash, hp);
}

static blk_pos * mins_search_hash(int lba)
{
	struct block_position *bp;
	struct hlist_node *hn;
	int hash = (lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];

	//printf("find %d, hash=%d hlist blkno=%d\n",lba,hash,cb->cb_blkno);
	hlist_for_each_entry(bp, hn, hp, hash) {
		if ( bp->lba == lba)
			return bp;
	} 
	dprintf("lba %d not in cache\n", lba);
	return NULL;
}

static void get_next_pos(struct cache_block *cb)
{
	blk_pos *bp;
	int next_pos = 0; 

	bp = mins_search_hash(cb->cb_blkno);
	assert(bp!=NULL);

	bp->next_idx ++;
	if(bp->next_idx <= bp->idx) {
		next_pos = bp->pos[bp->next_idx];
	}
	cb->cb_next_pos = next_pos;
}

/*find next use of new request with lba*/
static int find_next_pos(int lba)
{
	blk_pos *bp;
	int next_pos = 0; 

	bp = mins_search_hash(lba);
	assert(bp!=NULL);

	bp->next_idx ++;
	if(bp->next_idx <= bp->idx) {
		next_pos = bp->pos[bp->next_idx];
	}
	return next_pos;
}

void mins_scan_trace (cache_manager *cm, ioreq_event *new )
{
	int lba_start_align, lba_start, lba_end ;
	static int reqcount = 0 ;
	blk_pos *bp = NULL;
	
	lba_start = new->blkno;
	lba_start_align  = lba_start & ~(cm->cm_bsize/SECTOR_SIZE - 1);
	lba_end = new->blkno + new->bcount;

	dprintf("max_pos_len is %d\n",max_pos_len);
	for (;lba_start_align < lba_end; lba_start_align += cm->cm_bsize/SECTOR_SIZE) 
	{
		reqcount++;
		int lba = lba_start_align;
		if (( bp = mins_search_hash(lba)) == NULL ) {
			bp = calloc (1, sizeof(blk_pos));
			bp->lba = lba;
			bp->pos = calloc(REALLOC_STEP , sizeof(int));
			if(bp->pos == NULL) {
				printf("malloc error!\n");
				exit(-1);
			}
			bp->len = REALLOC_STEP;
			bp->pos[0] = reqcount;
			bp->idx	= 0;
			INIT_HLIST_NODE(&bp->hash);
			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		
			mins_insert_hash(bp);
		}
		else {
			if(bp->idx == (bp->len - 1)) {
				bp->pos = realloc(bp->pos, (bp->len + REALLOC_STEP) * sizeof(int));
				if(bp->pos == NULL) {
					printf("realloc error!\n");
					exit(-1);
				}
				bp->len += REALLOC_STEP;
			}
			bp->idx++;
			bp->pos[bp->idx] = reqcount;

			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		}
	}

}


/*if next use of new req is not earlier than all cache blocks, do not repalce any block*/
static struct cache_block * cache_replace(cache_manager *cm, int lba)
{
	struct cache_block *cb, *max=NULL;
	int max_pos = 1;
	int next_use_lba;

	/*find the cb with max nest_pos*/
	dprintf("In Cache Replace\n");
	list_for_each_entry(cb, &mins_list, cb_list) {
		dprintf("%llu nextpos=%d newlba=%d\n",cb->cb_blkno,cb->cb_next_pos,lba);
		if ( cb->cb_next_pos == 0){
			max = cb;
			break;
		}
		else if (cb->cb_next_pos > max_pos){
			max_pos = cb->cb_next_pos;
			max = cb;
		}
	}

	cb = max;

	next_use_lba = find_next_pos(lba)	;
	dprintf("%llu nextpos=%d  %d nextpos=%d\n",cb->cb_blkno,cb->cb_next_pos,lba,next_use_lba);
	if(cb->cb_next_pos) {
		if(next_use_lba == 0 || next_use_lba > cb->cb_next_pos)
			return NULL;
	}
	cb->cb_next_pos = next_use_lba;
	cm_remove_hash(cb);
	list_del_init(&cb->cb_list);
//	heap_delete(cb);
	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags))
		cm_flush_block (cb)	;
	else
		cm->cm_clean_count--;
	clear_bit(CB_RESIDENT, &cb->cb_flags);
//	printf("replace lba:%d\n",cb->cb_blkno);
	return cb;
}







static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;		
	dprintf("lba=%d, op=%d\n",lba,op);

	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		dprintf("cache hit lba=%d, op=%d\n",lba,op);
		if ( op == WRITE ) {
			if(test_bit(CB_DIRTY, &cb->cb_flags))        
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			cm->cm_write_hits++;
		}
		else {
			cm->cm_read_hits ++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}
//		heap_delete(cb);
//		heap_insert(cb);
		get_next_pos(cb);
	}
	else {
		if ((cb = cm_get_free_block(cm)) != NULL)
		{
			cb->cb_blkno = lba;
			get_next_pos(cb);
		}
		else
			cb = cache_replace(cm, lba);

		
		if(cb == NULL) {
		/*no replace*/
			dprintf("no replace lba=%d\n",lba);
			if(op == READ || fetch )	
				cm->cm_disk_reads++;
			if(op == WRITE )
				cm->cm_disk_writes++;
			return 1;
		}

		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
//		heap_insert(cb);
//		get_next_pos(cb);
		list_add(&cb->cb_list, &mins_list);

		if ( op == READ || fetch ) {
			cm_fetch_block(cb, lba);
		}
		if ( op == WRITE ) 
			cm_write_block(cb);
	}


	return 1;
}

static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}


struct cm_personality mins_personality = 
{
	.policy_name = "mins",
	.cache_read = cache_read,
	.cache_write = cache_write,
};



int mins_register(cache_manager *cm)
{
	cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;
	heapmax = calloc(cm->cm_size, sizeof(blk_pos *));
	hashtbl = (struct hlist_head *) calloc(MIN_HASH_NUM, sizeof(struct hlist_head));
	cm_personality_register(cm, &mins_personality);
	return 0;
}
