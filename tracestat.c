/*************************************************************************
    > File Name: min.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月27日 星期五 21时51分18秒
 ************************************************************************/

#include "cache_sim.h"
#include <getopt.h>
#define REALLOC_STEP	32



typedef struct block_position {
	struct hlist_node hash;
//	struct cache_block *cb;
	int lba;
	int idx;	/*the end idx*/
	int len;	/*length of pos now*/
	int next_idx;
	int heap_idx;	/*idx in the heapmax*/
	int *pos;

	struct list_head list;	/*link all the blks*/
	int read_count;
	int write_count;
	int total_count;
}blk_pos;

#define MIN_HASH_NUM	( 1024*1024)
#define MIN_HASH_MASK	( MIN_HASH_NUM - 1)

static LIST_HEAD(min_list);

static LIST_HEAD(blk_list);

static blk_pos * heapmax;
//static int heapsize=0;

static struct hlist_head *hashtbl;


static int max_pos_len = 0;
static int cache_bsize_sectors;

static void min_insert_hash(struct block_position *bp)
{
	int hash = (bp->lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];
	hlist_add_head(&bp->hash, hp);
}

static blk_pos * min_search_hash(int lba)
{
	struct block_position *bp;
	struct hlist_node *hn;
	int hash = (lba/cache_bsize_sectors) & MIN_HASH_MASK;
	struct hlist_head *hp = &hashtbl[hash];

	//printf("find %d, hash=%d hlist blkno=%d\n",lba,hash,cb->cb_blkno);
	hlist_for_each_entry(bp, hn, hp, hash) {
		if ( bp->lba == lba)
			return bp;
	} 
	dprintf("lba %d not in cache\n", lba);
	return NULL;
}

/*
static void get_next_pos(struct cache_block *cb)
{
	blk_pos *bp;
	int next_pos = 0; 

	bp = min_search_hash(cb->cb_blkno);
	assert(bp!=NULL);

	bp->next_idx ++;
	if(bp->next_idx <= bp->idx) {
		next_pos = bp->pos[bp->next_idx];
	}
	cb->cb_next_pos = next_pos;
}
*/



void tracestat_scan_trace (cache_manager *cm, ioreq_event *new )
{
	int lba_start_align, lba_start, lba_end ;
	static int reqcount = 0 ;
	blk_pos *bp = NULL;
	
	lba_start = new->blkno;
	lba_start_align  = lba_start & ~(cm->cm_bsize/SECTOR_SIZE - 1);
	lba_end = new->blkno + new->bcount;

	dprintf("max_pos_len is %d\n",max_pos_len);
	for (;lba_start_align < lba_end; lba_start_align += cm->cm_bsize/SECTOR_SIZE) 
	{
		reqcount++;
		int lba = lba_start_align;
		if (( bp = min_search_hash(lba)) == NULL ) {
			bp = calloc (1, sizeof(blk_pos));
			bp->lba = lba;
			bp->pos = calloc(REALLOC_STEP , sizeof(int));
			if(bp->pos == NULL) {
				printf("malloc error!\n");
				exit(-1);
			}
			bp->len = REALLOC_STEP;
			bp->pos[0] = reqcount;
			bp->idx	= 0;
			INIT_HLIST_NODE(&bp->hash);
			INIT_LIST_HEAD(&bp->list);
			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		
			min_insert_hash(bp);
		
			list_add(&bp->list,&blk_list);
		}
		else {
			if(bp->idx == (bp->len - 1)) {
				bp->pos = realloc(bp->pos, (bp->len + REALLOC_STEP) * sizeof(int));
				if(bp->pos == NULL) {
					printf("realloc error!\n");
					exit(-1);
				}
				bp->len += REALLOC_STEP;
			}
			bp->idx++;
			bp->pos[bp->idx] = reqcount;

			if (bp->len > max_pos_len )
				max_pos_len = bp->len;
		}
		if(new->flags == READ)
			bp->read_count++;
		else 
			bp->write_count++;
		bp->total_count++;
	}

}






void tracestat_print_blkstat(cache_manager *cm)
{

	blk_pos *bp;
	/*find the cb with max nest_pos*/

	list_for_each_entry(bp, &blk_list, list) {
		printf("%d %d %d %d\n",bp->lba, bp->read_count, bp->write_count,bp->total_count);
	}

}

FILE *tracefile = NULL;
int traceformat = MSR;
char *program_name;

void usage()
{
	printf("\
Usage: %s [OPTION]... TRACEFILE\n",program_name);
	printf("\
	-h, --help		show this help file\n\
	-a, --alpha		the alpha for zipf/pareto\n\
	-b, --blocksize[Bytes]	cache block size, should be power of 2\n\
	-s, --cachesize		number of cache blocks\n\
	-p, --policy		the name of the policy\n\
	-t, --type		the type of trace (from file, or synthetic)\n\
	-w, --workset[MB]	the working set for synthetic trace\n\
	-i, --intval		the interval for cache stats\n\
	-l, --lines		the lines of traces to read or gernerate\n\
	-r, --readratio		read ratio for synthetic trace[default:%.1f]\n\
	-m, --mode		0:WB, 1:RO, 2:WT\n\
	-A, --Architecture	0:ssd+backstore[default], 1:backstore, 2:ssd+hdd+backstore\n\
	-R, --Realtest		0:only for computing hit ratios[default],\n\
				1:real read/write and write dirty blocks to HDD\n\
				2:real read/write and write all cache blocks to HDD\n\
	-L, --Logshow		0: Do not show the log details [default]\n\
				1: Log dirty blocks to HDD \n\
				2: Log all cache blocks to HDD\n",SYNTH_READRATIO);
}

void parse(int argc, char **argv, cache_manager *cm){
	int opt;
	int dist_val_set = 0;
	struct option longopts[] = {
		{"blocksize",1,NULL,'b'},
		{"cachesize",1,NULL,'s'},
		{"policy",1,NULL,'p'},
		{"tracetype",1,NULL,'t'},
		{"mode",1,NULL,'m' },
		{"intval",1,NULL,'i' },
		{"workset",1,NULL,'w' },
		{"lines",1,NULL,'l' },
		{"alpha",1,NULL,'a' },
		{"readratio",1,NULL,'r'},
		{"Architecture",1,NULL,'A'},
		{"Realtest",1,NULL,'R'},
		{"Logshow",1,NULL,'L'},
		{GETOPT_HELP_OPTION_DECL},
		{0,0,0,0}};

	while ((opt = getopt_long(argc,argv, ":b:r:s:p:t:m:hi:w:l:a:R:A:L:", longopts, NULL)) != -1 ) {
		switch (opt) {
			case 'b':
				cm->cm_bsize = atoi(optarg);
				if (((cm->cm_bsize) & (cm->cm_bsize - 1))!= 0){
					fprintf(stderr, "the cache block size should be power of 2!\n");
					exit(-1);
				}
				break;
			case 's':
				cm->cm_size = atoi(optarg);
				break;
			case 'p':
				strcpy(cm->policy_name,optarg);
				break;
			case 'm':
				cm->cm_mode = atoi(optarg);
				break;
			case 't':
				strcpy(cm->tracetype, optarg);
				if(strcmp(optarg, "ascii")  == 0)
					traceformat = ASCII;
				else if (strcmp(optarg, "msr") == 0)
					traceformat = MSR;
				else if (strcmp(optarg,"zipf") == 0)
					traceformat = SYNTH_ZIPF;
				else if (strcmp(optarg,"pareto") == 0)
					traceformat = SYNTH_PARETO;
				else
					traceformat = -1;
				break;
			case 'r':
				cm->cm_synth_read_ratio = atof(optarg);
				break;
			case 'a':
				cm->cm_alpha = atof(optarg);
				dist_val_set = 1;
				break;
			case 'i':
				cm->cm_stat_intval = atoi(optarg);
				break;
			case 'w':
				cm->cm_working_set = atoi(optarg);
//				printf("set working set to %d\n", cm->cm_working_set);
				break;
			case 'l':
				cm->cm_trace_lines = atoi(optarg);
				break;
			case 'A':
				cm->arch = atoi(optarg);
				break;
			case 'R':
				cm->realtest = atoi(optarg);
				printf("set realtest to %d\n",cm->realtest);
				break;
			case 'L':
				cm->logshow = atoi(optarg);
				break;
			case 'h':
				usage();
				exit(-1);
				break;
			case ':':
				printf("option needs a value\n");
				break;
			case '?':
				printf("Unknown option: %c\n",optopt);
				break;
		}
	}
//	printf("argc=%d, optind=%d\n",argc,optind);
	cm->traceformat = traceformat;
//	SYNTH_ZIPF is the first synthetic trace 
	if ( traceformat < SYNTH_ZIPF ) {	/*It's not a synthetic trace'*/
		if( argc-1 < optind ){
			fprintf(stderr, "miss the trace file\n");
			usage();
			exit(-1);
		}
	//	printf("the tracefile is %s\n",argv[optind]);
		if( (tracefile = fopen(argv[optind],"r")) == NULL){
			fprintf(stderr, "open file %s error!\n", argv[optind]);
			exit(-1);
		}
		cm->tracefile = tracefile;
		strcpy(cm->tracefilename, argv[optind]);
	}
	else {		/*for synthetic trace*/
		/* Use cm_bsize as unit, when generate LBA, 
		 * it should be multiped by (cm_bsize/SECTOR_SIZE)*/
		unsigned long nranges = (WORKSET_UNIT/cm->cm_bsize) * cm->cm_working_set;
		//printf("nranges = %lu\n",nranges);
		strcpy(cm->tracefilename, "Synthetic");
		if(traceformat == SYNTH_ZIPF) {
			if(!dist_val_set)
				cm->cm_alpha = DEF_ZIPF_VAL;
			else if(cm->cm_alpha == 1.0) {
				printf("zipf input must be different than 1.0\n");
				exit(-1);
			}
			zipf_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
		else if(traceformat == SYNTH_PARETO) {
			if (!dist_val_set)
				cm->cm_alpha = DEF_PARETO_VAL;
			else if (cm->cm_alpha >= 1.00 || cm->cm_alpha < 0.00) {
				printf("pareto input must be > 0.00 and < 1.00\n");
				exit(-1);
			}
			pareto_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
	}
}

int main(int argc, char **argv)
{

	int reqcount = 0;
	ioreq_event *new=NULL;
	cache_manager *cm=NULL;
	program_name = (char *) argv[0];
	if (argc < 2){
		printf("args are missed\n");
		printf("sizeof off_t is %d\n",sizeof(off_t));
		usage();
		return -1;
	}
	cm = cm_init(cm);
	parse(argc,argv,cm);
	//traceformat = cm->traceformat;
	//show_cache_config(cm);

	cm_setup(cm);

	cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;
	heapmax = calloc(cm->cm_size, sizeof(blk_pos *));
	hashtbl = (struct hlist_head *) calloc(MIN_HASH_NUM, sizeof(struct hlist_head));

	new = malloc(sizeof(ioreq_event));
	assert(new!=NULL);

	while ((new=iotrace_get_ioreq_event(cm,traceformat,new))!= NULL ){
		reqcount++;
		tracestat_scan_trace(cm, new);
		if (reqcount >= cm->cm_trace_lines)
			break;
	}

	if(traceformat < SYNTH_ZIPF) fclose(tracefile);

	tracestat_print_blkstat(cm);
	cache_exit(cm);
	return 0;
}
