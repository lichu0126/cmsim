/*************************************************************************
    > File Name: cache_sim.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月21日 星期六 20时38分17秒
 ************************************************************************/

#include "cache_sim.h"

void show_cache_config(cache_manager *cm)
{
	const char cache_mode[][50] = {
		"Write Back", "Read Only(Write Around)", "Write Through",
	};
	printf("*********Cache Configuration**************\n");
	printf("cache block size is:\t%d\n",cm->cm_bsize);
	printf("cache total size is:\t%d KB\n",(cm->cm_bsize/1024) * cm->cm_size);
	printf("the cache policy is:\t%s\n",cm->policy_name);
	printf("the cache mode is:\t%s\n",cache_mode[cm->cm_mode]);
	printf("the stat interval is:\t%d\n",cm->cm_stat_intval);
	printf("the traceformat is:\t%s\n",cm->tracetype);
	printf("the tracefile is:\t%s\n",cm->tracefilename);
	printf("total tracing lines:\t<=%d\n", cm->cm_trace_lines);
	printf("the working set is:\t%d\n",cm->cm_working_set);
	printf("the synth read ratio:\t%.3f\n",cm->cm_synth_read_ratio);
	printf("the synth alpha:\t%.3f\n",cm->cm_alpha);
	printf("******************************************\n\n");
	return;
}

cache_manager * cm_init(cache_manager *cm)
{
	int i;
	cm = calloc(1,sizeof(cache_manager));

	INIT_LIST_HEAD(&cm->cm_free_list);
	cm->pers = NULL;
	cm->cm_bsize = CACHE_BSIZE;
	cm->cm_size = CACHE_SIZE;
	cm->cm_mode = CACHE_MODE_WB;
	cm->cm_stat_intval = STAT_INTVAL; 
	cm->cm_trace_lines = TRACE_LINES;
	cm->cm_working_set = WORKING_SET;
	cm->cm_synth_read_ratio = SYNTH_READRATIO;

	cm->logshow  = 0;
	cm->log_tail = 0;

	strcpy(cm->policy_name,"lru");
	strcpy(cm->tracetype, "msr");
	cm->cm_hashtbl = (struct hlist_head *)malloc(sizeof(struct hlist_head) * HASH_NUM);
    if(cm->cm_hashtbl == NULL){
        fprintf(stderr, " Malloc Error %s %d \n",__FUNCTION__,__LINE__);
        exit(1);
    }

    for(i = 0;i < HASH_NUM;i++){
        INIT_HLIST_HEAD ( &cm->cm_hashtbl[i] );
    }

	/*For Log,   fixed later*/
	int lseg_header_num = SEGMENT_NUM;
	int j;
	lseg_header *lsh;
	cm->lsegtbl = (lseg_header *)malloc(sizeof(lseg_header) * lseg_header_num);
    if(cm->lsegtbl == NULL){
        fprintf(stderr, " Malloc Error %s %d \n",__FUNCTION__,__LINE__);
        exit(1);
    }
	for(i=0; i< lseg_header_num; i++){
		lsh = cm->lsegtbl+i;
		lsh->segment_id = i;
		lsh->valid_count = 0;
		for (j=0; j< SEGMENT_SIZE;  j++)
			lsh->slot_lba_table[j] = -1;
	}
	cm->curr_seg_id = 0;
	cm->curr_slot_id = 0;
	return cm;
}

/*For Log*/
void cm_log_print(cache_manager *cm)
{
	int i, flag=0;
	lseg_header *lsh;
	int total_valid_count = 0;

	for (i= 0; i< SEGMENT_BINS; i++)
		cm->seg_bins[i] = 0 ;
	
	/* Note: Now it is assumed SEGMENT_NUM is large enough,
	 * and loghead is always larger than logtail.
	 * Maybe fixed later
	 * */
	for(i=0; i<= cm->curr_seg_id ; i++){
		lsh = cm->lsegtbl+i;
		total_valid_count += lsh->valid_count;
		/*
		printf("%-3d",lsh->valid_count);
		if( (i+1)%20 == 0 )
			printf("\n");
		*/
		/*reach the body of log*/
		if(lsh->valid_count!=0 && !flag){
			cm->log_tail = i;
			flag = 1;
		}
		/*stats the body of log*/
		if( flag ) {
			int vcount=lsh->valid_count;
			int bin_index = 0;
			int step = SEGMENT_SIZE/(SEGMENT_BINS-1);

			bin_index = (vcount+step-1)/step;
			assert(bin_index<SEGMENT_BINS);
			cm->seg_bins[bin_index]++;
			//printf("valid_count=%d index=%d\n",lsh->valid_count,bin_index);
		}
	}
	/*This can happen when cache is very small and using L1 mode*/
	if( flag == 0 )	/*In fact, now LogSize == 0*/
		cm->log_tail = cm->curr_seg_id;
	printf("Log tail=%d, head=%d, size=%d\n",cm->log_tail,cm->curr_seg_id,(cm->curr_seg_id-cm->log_tail+1)*SEGMENT_SIZE);
	printf("Total Valid Count: %d\n",total_valid_count);
	printf("Segment Histogram: ");
	printf("[%d] ",cm->seg_bins[0]);
	for (i = 1 ; i< SEGMENT_BINS; i++)
		printf("%d ",cm->seg_bins[i]);
	printf("\n");
}

void cache_exit(cache_manager *cm)
{
	cm->pers = NULL;
	if( cm->logshow ) {
		cm_log_print(cm);
	}

	free(cm->cm_hashtbl);
	free(cm->lsegtbl);
	free(cm);
}

struct cache_block * cm_alloc_block(struct cache_manager *cm) 
{
	struct cache_block *cb = NULL;

	cb = calloc(1,sizeof(struct cache_block));
	if(cb == NULL) {
		fprintf(stderr,"alloc cache block error!\n");
		return NULL;
	}
	cb->cb_cm = cm;;
	INIT_LIST_HEAD(&cb->cb_list);

	cb->cb_seg_id = -1 ;
	cb->cb_slot_id = -1;
	return cb;
}

struct cache_block * cm_reset_block(cache_manager *cm, struct cache_block *cb)
{
//	memset(cb, 0, sizeof(struct cache_block));
	cb->cb_blkno = -1;
	cb->cb_flags = 0;
	cb->cb_list_idx = NA;
	cb->cb_cm = cm;
	cb->cb_freq = 0;
	INIT_LIST_HEAD(&cb->cb_list);

	cb->cb_seg_id = -1 ;
	cb->cb_slot_id = -1;
	return cb;
}

void cm_setup(cache_manager *cm) {
	/*Alloc cm_size num of cache block, and add to cm_free_list*/
	int num = cm->cm_size;
	struct cache_block *cb = NULL;

	cm->cm_free_count = 0;
	while(num--){
		cb = cm_alloc_block(cm);
		//cb->cb_isghost = 0;
		cm->cm_free_count++;
		list_add_tail(&cb->cb_list,&cm->cm_free_list);
	}

	/* Register the cache policy
	 * It's easy to add new policies here.
	 * */
	if(strcmp(cm->policy_name, "lru") == 0){
		lru_register(cm);
	}
	else if(strcmp(cm->policy_name, "arc") == 0){
		arc_register(cm);
	}
	else if(strcmp(cm->policy_name, "harc") == 0){
		harc_register(cm);
	}
	else if(strcmp(cm->policy_name, "lruwsr") == 0){
		lruwsr_register(cm);
	}
	else if(strcmp(cm->policy_name, "ccflru") == 0){
		ccflru_register(cm);
	}
	else if(strcmp(cm->policy_name, "min") == 0){
		min_register(cm);
	}
	else if(strcmp(cm->policy_name, "cflru") == 0){
		cflru_register(cm);
	}
	else if(strcmp(cm->policy_name, "fifo") == 0){
		fifo_register(cm);
	}
	else if(strcmp(cm->policy_name, "plfu") == 0){
		plfu_register(cm);
	}
	else if(strcmp(cm->policy_name, "faplfu") == 0){
		faplfu_register(cm);
	}
	else if(strcmp(cm->policy_name, "mins") == 0){
		mins_register(cm);
	}
	else{
		fprintf(stderr,"%s policy has not been supported now!\n",cm->policy_name);
		exit(-1);
	}

	/*Add by lichu@2017-7-17 for Real Read/Write*/
	if ( cm->realtest == 0 )
		return;
	if( (cm->fd_hdd = open(HDD_DEV, O_RDWR|O_DIRECT)) == -1 ){
		log_err("open %s error!\n",HDD_DEV);
		goto err;
	}
	if( (cm->fd_ssd = open(SSD_DEV, O_RDWR|O_DIRECT)) == -1 ){
		log_err("open %s error!\n",SSD_DEV);
		goto err;
	}
	if( (cm->fd_backstore = open(BACK_DEV, O_RDWR|O_DIRECT)) == -1 ){
		log_err("open %s error!\n",BACK_DEV);
		goto err;
	}

	cm->ssd_size = lseek(cm->fd_ssd, 0, SEEK_END);
	cm->hdd_size = lseek(cm->fd_hdd, 0, SEEK_END);
	cm->back_size = lseek(cm->fd_backstore, 0, SEEK_END);
	lseek64(cm->fd_ssd, 0, SEEK_SET);
	lseek64(cm->fd_hdd, 0, SEEK_SET);
	lseek64(cm->fd_backstore, 0, SEEK_SET);

	printf("ssd size:%jd MB\n",cm->ssd_size/1048576);
	printf("hdd size:%jd MB\n",cm->hdd_size/1048576);
	printf("backstore size:%jd MB\n",cm->back_size/1048576);

	size_t page_mask = sysconf(_SC_PAGESIZE) - 1;
	void *io_buffer = malloc(cm->cm_bsize + page_mask);
	if ( io_buffer == NULL) {
		log_err("malloc io_buffer error!\n");
		goto err;
	}
	/*align to PAGESIZE*/
	cm->io_buffer = (void *)(((uintptr_t)io_buffer + page_mask) & ~page_mask);
	return;
	/**/
err:
	exit(-1);
}

struct cache_block * cm_get_free_block(cache_manager *cm)
{
	struct cache_block *cb = NULL;
	struct list_head *first;
	if(list_empty(&cm->cm_free_list))
		goto out;
	first = cm->cm_free_list.next;
	cb = list_entry(first,struct cache_block, cb_list);
	list_del_init(first);
	cb->cb_list_idx = NA;
out:
	return cb;
}

void cm_personality_register(cache_manager *cm, struct cm_personality *p)
{
	cm->pers = p;	
}

/*
int cm_read(cache_manager *cm, int lba, int bcount)
{
			
}
*/

static inline struct hlist_head * cache_hash(cache_manager *cm, int lba)
{
	int cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;
	int hash = (lba/cache_bsize_sectors) & HASH_MASK;
	return &cm->cm_hashtbl[hash];
}

void cm_insert_hash(cache_manager *cm, struct cache_block *cb)
{
	struct hlist_head *hp ;

	hp = cache_hash(cm, cb->cb_blkno);
	hlist_add_head(&cb->cb_hash, hp);
}

void cm_remove_hash(struct cache_block *cb)
{
	hlist_del_init(&cb->cb_hash);
}

struct cache_block * cm_search_hash(cache_manager *cm, int lba)
{
	struct cache_block *cb;
	struct hlist_node *hn;
	struct hlist_head *hp = cache_hash(cm, lba);

	hlist_for_each_entry(cb, hn, hp, cb_hash) {
		//printf("find %d, hash=%d hlist blkno=%d\n",lba,hash,cb->cb_blkno);
		if ( cb->cb_blkno == lba)
			return cb;
	} 

	dprintf("lba %d not in cache\n", lba);
	return NULL;
}

static void cm_invalidate(cache_manager *cm, int lba){
	struct cache_block *cb;
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		if(test_bit(CB_RESIDENT, &cb->cb_flags)) {	/*not a ghost block*/
			cm_remove_hash(cb);
			list_del_init(&cb->cb_list);
			list_add(&cb->cb_list, &cm->cm_free_list);
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_count--;
			else
				cm->cm_clean_count--;
			cb->cb_flags = 0;
		}
	}
}

/* This is just a writethough after Writeback Mode,
 * thus all other policies don't need deal with WT mode*/
static void cm_writethrough(cache_manager *cm, int lba){
	struct cache_block *cb;
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		cm_disk_write(cm);
		clear_bit(CB_DIRTY, &cb->cb_flags);
		cm->cm_dirty_count--;
		cm->cm_clean_count++;
	}
	else	/* Should not be here, because a writeback has happened*/
		fprintf(stderr,"something wrong in writethrough\n");
}

static inline double now(void)                                                  
{
    struct timeval tv; 
    gettimeofday(&tv, 0); 
    return tv.tv_sec + 1e-6*tv.tv_usec;
}

int cm_access(cache_manager *cm, ioreq_event *new)
{
	unsigned int lba_start_align, lba_start, lba_end ;
	int cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;
	static int reqcount = 0 ;
	int ret;

	if(reqcount == 0) {
		cm->stat_last = now();
		cm->stat_curr = cm->stat_last;
	}

	
	lba_start = new->blkno;
	lba_start_align  = lba_start & ~(cm->cm_bsize/SECTOR_SIZE - 1);
	lba_end = new->blkno + new->bcount;

	for (;lba_start_align < lba_end; lba_start_align += cm->cm_bsize/SECTOR_SIZE) 
	{
		/*This means not using Any Cache*/
		if (test_bit(BIT_NO_SSD,&cm->arch) && \
				!test_bit(BIT_ADD_HDD,&cm->arch)) {
			assert(cm->realtest != 0);
			if(new->flags == READ) {
				ret = pread(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,lba_start_align*SECTOR_SIZE);
			}
			else {
				ret = pwrite(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,lba_start_align*SECTOR_SIZE);
			}
			if(ret != cm->cm_bsize) {
				perror("cm_access error:");
				exit(1);
			}
		}
		else if (new->flags == READ) {	/*Read request*/
	//		double t = now();
			cm->cm_read_count ++;
			CACHE_READ (cm, lba_start_align);
			dprintf("cache read latency is %.6f\n",now()-t);
		}
		else{	/*Write request*/
			/*the first and last block which is not OVERWRITE, need FETCH_ON_WRITE */
			int fetch = 0;
			if(lba_start > lba_start_align || (lba_start_align + cache_bsize_sectors > lba_end ))
				fetch = 1;

			cm->cm_write_count ++;
			if (cm->cm_mode == CACHE_MODE_RO){
				cm_disk_write(cm);
				cm_invalidate(cm, lba_start_align);
			}
			else {
				CACHE_WRITE(cm, lba_start_align, fetch);
				if(cm->cm_mode == CACHE_MODE_WT) {
					cm_writethrough(cm,lba_start_align);

					if(!PARALLEL_WT) {
					/*write to Backing Store now*/
						int ret;
//						double t=now();
	//					printf("%.6f write backing blkno=%d\n",t,lba_start_align);
						ret = pwrite(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,lba_start_align*SECTOR_SIZE);
						if(ret != cm->cm_bsize) {
							perror("cm_access error:");
							exit(1);
						}
	//					printf("dbg latency array is %.6f\n",now()-t);
					}
				}
			}
		}
//		printf("reqcount=%d intval=%d INTVAL=%d\n",reqcount,cm->cm_stat_intval,STAT_INTVAL);
		/*Print the Stats for cache hit, etc...*/
//		usleep(10000);
	}

	if( (++reqcount) % (cm->cm_stat_intval) == 0 ) {
		printf("******Cache Stats index=%d******\n",reqcount/cm->cm_stat_intval);
		cm_print_stat(cm);
		cm_reset_stat(cm);
		if(cm->logshow)
			cm_log_print(cm);
		printf("\n");
	}

	return 0;
}

int cm_disk_write(cache_manager *cm) 
{
	cm->cm_disk_writes++;
	return 0;
}

int cm_disk_read(cache_manager *cm)
{
	cm->cm_disk_reads++;
	return 0;
}

void cm_reset_stat(cache_manager *cm)
{
	cm->cm_read_count = 0;
	cm->cm_write_count = 0;
	cm->cm_read_hits = 0;
	cm->cm_write_hits = 0;
	cm->cm_dirty_write_hits = 0;
	cm->cm_dirty_read_hits = 0;
	cm->cm_disk_reads = 0;
	cm->cm_disk_writes = 0;
	cm->stat_last = cm->stat_curr;
}

void cm_print_stat(cache_manager *cm)
{
	printf("total read	: %d\t%.3f\n",cm->cm_read_count, (double)cm->cm_read_count/(double)(cm->cm_read_count + cm->cm_write_count));
	printf("total write	: %d\t%.3f\n",cm->cm_write_count,(double)cm->cm_write_count/(double)(cm->cm_read_count + cm->cm_write_count));
	printf("read  hits	: %d\t%.3f\n",cm->cm_read_hits,(double)cm->cm_read_hits/(double)(cm->cm_read_count+cm->cm_write_count));
	printf("dirty read hits	: %d\t%.3f\n",cm->cm_dirty_read_hits,(double)cm->cm_dirty_read_hits/(double)(cm->cm_read_count+cm->cm_write_count));
	printf("write hits	: %d\t%.3f\n",cm->cm_write_hits,(double)cm->cm_write_hits/(double)(cm->cm_read_count+cm->cm_write_count));
	printf("dirty write hits	: %d\t%.3f\n",cm->cm_dirty_write_hits,(double)cm->cm_dirty_write_hits/(double)(cm->cm_read_count+cm->cm_write_count));
	printf("total hits	: %d\t%.3f\n",cm->cm_write_hits+cm->cm_read_hits,(double)(cm->cm_write_hits+cm->cm_read_hits)/(double)(cm->cm_read_count+cm->cm_write_count));
	printf("disk read	: %d\n",cm->cm_disk_reads);
	printf("disk write	: %d\n",cm->cm_disk_writes);

	printf("clean count	: %d\n",cm->cm_clean_count);
	printf("dirty count	: %d\n",cm->cm_dirty_count);
	if(cm->realtest) {
		cm->stat_curr = now();
		printf("latency is %.3f s\n",cm->stat_curr-cm->stat_last);
	}
}

/*Read data from lower level storage */
int cm_fetch_block( struct cache_block *cb, int lba) 
{
	int ret;
	struct cache_manager *cm = cb->cb_cm;
	cb->cb_blkno = lba;
	set_bit(CB_RESIDENT, &cb->cb_flags);
	clear_bit(CB_DIRTY, &cb->cb_flags);
	cm->cm_disk_reads++;
	cm->cm_clean_count++;

	if(cm->realtest) {
		ret = pread(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,cb->cb_blkno*SECTOR_SIZE);
		//ret = cm->cm_bsize;
		if (ret != cm->cm_bsize)
			goto err;
	}
	return 1;
err:
	if(ret == 0 ) {
		printf("fetch out of scope\n");
		return 1;
	}
	perror("cm_fetch_block error:");
	exit(1);
}

/*flush dirty block to lower level storage*/
int cm_flush_block ( struct cache_block *cb ) {
	struct cache_manager *cm = cb->cb_cm;
	cm->cm_disk_writes++;
	cm->cm_dirty_count--;

	if(cm->realtest) {
	/* 1. read from cache
	 * 2. write to backing store*/
		int ret;
		ret = pread (cm->fd_ssd,cm->io_buffer,cm->cm_bsize,cb->cb_ssd_off*SECTOR_SIZE);
		if (ret != cm->cm_bsize)
			goto err;
//		ret = pwrite(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,cb->cb_blkno*SECTOR_SIZE);
		ret =cm->cm_bsize;
		if (ret != cm->cm_bsize)
			goto err;
	}
	return 0;
err:
	perror("cm_flush_block error");
	exit(1);
}

/* write data from upper level to cb
 * will be called in CACHE_WRITE*/
int cm_write_block ( struct cache_block *cb ) {
	struct cache_manager *cm = cb->cb_cm;
	if ( !test_and_set_bit (CB_DIRTY, &cb->cb_flags)) {
		cm->cm_dirty_count++ ;
		/* In case of get_free_block/alloc_block without 
		 * fetch_block, the clean_count is not reduced  */
		if ( test_and_set_bit(CB_RESIDENT, &cb->cb_flags))
			cm->cm_clean_count-- ;
	}
	return 0;
}



/* Added by lichu@2014-07-17 For Real Read/Write*/
void * real_write2cache_engine (void *arg) 
{
	struct cache_block *cb = (struct cache_block *) arg;
	cache_manager *cm = cb->cb_cm;
	int ret = 0 ;

	dprintf("thread_id=%d count=%d\n",pthread_self(),cb->cb_atomic_count);

	if( __sync_add_and_fetch(&cb->cb_atomic_count,1) == 1 ) {
		if(!test_bit(BIT_NO_SSD, &cm->arch)) {
			/*Record the current offset in SSD, Log Structured way*/
			cb->cb_ssd_off = lseek(cm->fd_ssd, 0, SEEK_CUR);
			if(cb->cb_ssd_off >= cm->cm_size*cm->cm_bsize)
				cb->cb_ssd_off = lseek(cm->fd_ssd, 0, SEEK_SET);
			cb->cb_ssd_off/=SECTOR_SIZE;
			/*write to SSD*/
			ret = write(cm->fd_ssd, cm->io_buffer, cm->cm_bsize);
			dprintf("thread_id=%lu count=%d write to ssd ret=%d\n",pthread_self(),cb->cb_atomic_count,ret);
			if(ret != cm->cm_bsize) {
				perror("write ssd error");
				exit(1);
			}
		}
	}
	else if( cb->cb_atomic_count == 2 ) {
		/*Note: in WT mode, we don't consider using HDD as cache*/
		if(cm->cm_mode == CACHE_MODE_WT ) {
			/*write to Backing Store if using PARALLEL_WT mode*/
			if (!test_bit(CB_OP, &cb->cb_flags) && PARALLEL_WT)	/*it's a WRITE operation*/
			{
				double t;
				t = now();
				printf("%.6f write backing blkno=%jd\n",t,cb->cb_blkno);
				ret = pwrite(cm->fd_backstore,cm->io_buffer,cm->cm_bsize,cb->cb_blkno*SECTOR_SIZE);
				dprintf("thread_id=%lu count=%d write to ssd ret=%d\n",pthread_self(),cb->cb_atomic_count,ret);
				if(ret != cm->cm_bsize) {
					perror("write backstore error");
					exit(1);
				}
				printf("%.6f write backing latency is %.6f\n",now(),now()-t);

			}

		}
		else if(test_bit(BIT_ADD_HDD,&cm->arch) ) {
			/*write to HDD*/
			ret = write(cm->fd_hdd, cm->io_buffer, cm->cm_bsize);
			dprintf("thread_id=%d count=%d write to hdd ret=%d\n",pthread_self(),cb->cb_atomic_count,ret);
		}
	}
	//if(ret != cm->cm_bsize) {
	//	log_err("write error in cm_real_write! ret=%d\n",ret);
	//	perror("Error: ");
	//	exit(-1);
	//	fprintf(stdout,"error in %s ret=%d\n",__func__,ret);
	//}

	return NULL;
}

/* Added by lichu@2014-07-17 For Real Read/Write to cache
 * if nth=1 It means only write to SSD;
 * if nth=2 We write to SSD cache and HDD mirror concurrently*/
void real_write2cache(struct cache_block *cb, int nth) 
{
	cache_manager *cm = cb->cb_cm;
	if ( cm->realtest != 0 ) {
		int nr_threads=nth;
		int i;
		pthread_t  tid[nr_threads];
		//double t=now();
		dprintf("%.6f pthread_create\n",t);
		for ( i = 0 ; i < nr_threads; i++) {
			if (pthread_create(&tid[i],NULL,real_write2cache_engine,cb)) {
				log_err("pthread_create error!\n");
				exit(1);
			}
		}
		for ( i = 0 ; i < nr_threads; i++) {
			pthread_join(tid[nr_threads-1-i],NULL);
//			pthread_join(tid[i],NULL);
			
			/*for dbg
			if( !test_bit(CB_OP, &cb->cb_flags)) {
				if(i == 0)
					printf("dbg latency ssd is %.6f\n",now()-t);
				else if (cm->cm_mode == CACHE_MODE_WT)
					printf("dbg latency array is %.6f\n",now()-t);
				else
					printf("dbg latency hdd is %.6f\n",now()-t);
			}
			*/
			
		}
		cb->cb_atomic_count = 0;	/*clear it to 0, maybe should be sync...*/
	}
}

void real_read_ssd(struct cache_block *cb)
{	 
	int ret = 0;
	cache_manager *cm = cb->cb_cm;
	//printf("cb->cb_ssd_off=%llu,ret=%d,bsize=%d\n",cb->cb_ssd_off,ret,cm->cm_bsize);
	ret = pread (cm->fd_ssd,cm->io_buffer,cm->cm_bsize,cb->cb_ssd_off*SECTOR_SIZE);
	if (ret != cm->cm_bsize) {
		//perror("read ssd error:");
		//exit(1);
		printf("cb->cb_ssd_off=%jd,ret=%d,bsize=%d\n",cb->cb_ssd_off,ret,cm->cm_bsize);
	}
}





/* For Log */

/*
 * Only remove a log entry (when replace)
 * */
int cm_log_remove(struct cache_block *cb)
{
	cache_manager *cm = cb->cb_cm;
	lseg_header *lsh;

	/*invalidate old slot*/
	if( cb->cb_seg_id != -1 && cb->cb_slot_id != -1 ) {
		assert(cb->cb_slot_id < SEGMENT_SIZE);
		lsh = cm->lsegtbl + cb->cb_seg_id;
		if (lsh->slot_lba_table[cb->cb_slot_id] != -1) {
			lsh->slot_lba_table[cb->cb_slot_id] = -1;
			lsh->valid_count --;
		}
	}
	cb->cb_seg_id = -1;
	cb->cb_slot_id = -1;
	return 0;
}

/*
 * First remove old entry, then add the new one
 */
int cm_log_insert(struct cache_block *cb)
{
	cache_manager *cm = cb->cb_cm;
	int seg_id  = cm->curr_seg_id;
	int slot_id = cm->curr_slot_id;
	lseg_header *lsh;

	/*invalidate old slot*/
	cm_log_remove(cb);
	/*log new lba*/
	lsh = cm->lsegtbl + seg_id;
	lsh->slot_lba_table[slot_id] = cb->cb_blkno;
	lsh->valid_count++;
	if( ++cm->curr_slot_id == SEGMENT_SIZE) {
		/*This segment has been full filled*/
		cm->curr_seg_id++;
		cm->curr_slot_id = 0;
	}
	cb->cb_seg_id = seg_id ;
	cb->cb_slot_id = slot_id;
	return 0;
}
