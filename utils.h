/*************************************************************************
    > File Name: utils.h
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月21日 星期六 20时12分04秒
 ************************************************************************/
#ifndef  __UTILS_H__
#define  __UTILS_H__


#ifdef DBG
#define dprintf(fmt, arg...) do { \
        printf("@%s:%d\t",__func__,__LINE__); \
        printf(fmt,##arg); \
        }while(0)
#else
#define dprintf(fmt, arg...)
#endif

#ifndef min
#define min(a, b)	((a) < (b) ? (a) : (b))
#endif
#ifndef max
#define max(a, b)	((a) > (b) ? (a) : (b))
#endif


/*bit opts*/
static inline void set_bit(int nr, volatile unsigned int *addr) 
{
	*addr |= (1U << nr);
}

static inline void clear_bit(int nr, volatile unsigned int *addr) 
{
	*addr &= ~(1U << nr);
}

static inline int test_bit(int nr, volatile unsigned int *addr) 
{
	return 1U & (*addr >> nr);
}

static inline int test_and_set_bit(int nr, volatile unsigned int *addr)
{
	int set;
	set = (*addr & (1U << nr) )? 1 : 0;
	if (!set)
		*addr |= (1U << nr);
	return set;
}

static inline int test_and_clear_bit(int nr, volatile unsigned int *addr)
{
	int set;
	set = (*addr & (1U << nr) )? 1 : 0;
	if (set)
		*addr &= ~(1U << nr);
	return set;
}

#endif
