#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../sim

#./test.sh trace.list 
Policy=(harc)
PolicyCount=${#Policy[@]}
CacheSizeMin=4096
CacheSizeMax=4096
while read tracefile
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi
	CacheSize=$CacheSizeMin
	while [ $CacheSize -le $CacheSizeMax ]
	do
		for((i=0; i<PolicyCount; i++))
		do
			echo "Policy=${Policy[i]}, CacheSize=$CacheSize, trace=$TRACEDIR/$tracefile"
			trace=`basename $tracefile`
			log=${trace%%\.*}_$LINES"_"${Policy[i]}.log
			$PROGRAM -s $CacheSize -i20000 -p ${Policy[i]} $TRACEDIR/$tracefile -l $LINES>>	$log
			echo $log
		done

		((CacheSize *= 2))
	done

done < $1
