#########################################################################
# File Name: display_all.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年07月29日 星期二 12时13分51秒
#########################################################################
#!/bin/bash
if [ $# != 1 ]; then
	echo "usage: $0 prefix_of_png"
	exit
fi

for png in *$1.png
do
	display $png &
done
