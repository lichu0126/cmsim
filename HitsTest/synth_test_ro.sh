#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=sim
Policy=(lru min)
PolicyCount=${#Policy[@]}
#./test.sh trace.list 

while read syntharg
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi
	CacheSize=$CacheSizeMin
	while [ $CacheSize -le $CacheSizeMax ]
	do
		for((i=0; i<PolicyCount; i++))
		do
			echo "Policy=${Policy[i]}, CacheSize=$CacheSize"
			log=synthtest_ro.log
			echo $syntharg
			$PROGRAM -s $CacheSize -i90000000 -p ${Policy[i]} -m1 $syntharg >>	$log
			echo $log
		done

		((CacheSize *= 2))
	done

done < $1
