#########################################################################
# File Name: display_all.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年07月29日 星期二 12时13分51秒
#########################################################################
#!/bin/bash
if [ $# -lt 1 ]; then
	echo "usage: $0 prefix_of_png"
	exit
fi

#for png in *$1*40*.png
for png in `ls $1*$2.png|sort`
do
	echo $png
	display $png &
	sleep 1
done
