#########################################################################
# File Name: stat.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 13时03分45秒
#########################################################################
#!/bin/bash

source common.sh

row=0
gap=0

LOGFILE=$1
Policy=(lru)
#echo ${Policy[@]}
#grep "total hits" $1 |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
#grep "dirty write hits" $1 |awk '{print $6}'|tr '\n' ' '|xargs -n $PolicyCount 

function gethits {
	echo total_hits
	grep "total hits" $LOGFILE |awk '{print $5}'
}
#"***************read hits**********"
function getreadhits {
	echo read_hits
	grep "read  hits" $LOGFILE |awk '{print $5}'
}

function getwritehits {
	echo write_hits
	grep "^write hits" $LOGFILE |awk '{print $5}'
}
function getdirtywritehits {
	echo dwrhits
	grep "dirty write hits" $LOGFILE |awk '{print $6}'
}
function getdirtyreadhits {
	echo drhits
	grep "dirty read hits" $LOGFILE |awk '{print $6}'
}
function getreadrate {
	echo readrate
	grep "^total read" $LOGFILE |awk '{print $5}'
}
function getcleanpct {
	echo clean_pct
	#Note: 4096 means cache size (num of blocks), should change if necassiry
	grep "^clean count" $LOGFILE|awk '{printf "%0.3f\n",$4/4096}'
}

#echo ${Policy[@]} "***************disk read**********"
#grep "disk read" $LOGFILE |awk '{print $4}'|tr '\n' ' '|xargs -n $PolicyCount 
function getdiskwrite {

	grep "disk write" $LOGFILE |awk '{print $4}'
}

DATFILE=${1%\.*}.dat

for i in `seq 1 7`; 
do 
	TMPF[$i]=/dev/shm/$RANDOM; 
done
gethits > ${TMPF[1]}
getreadhits > ${TMPF[2]}
getwritehits > ${TMPF[3]}
getdirtyreadhits > ${TMPF[4]}
getdirtywritehits > ${TMPF[5]}
getreadrate > ${TMPF[6]}
getcleanpct > ${TMPF[7]}

paste ${TMPF[@]} > hits_$DATFILE
rm -f ${TMPF[@]}
