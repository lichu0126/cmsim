#########################################################################
# File Name: singletest.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月29日 星期日 14时00分45秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../sim

Policy=("fifo")
PolicyCount=${#Policy[@]}
CacheSizeMin=1024
CacheSizeMax=1024
while read tracefile
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi
	CacheSize=$CacheSizeMin
	while [ $CacheSize -le $CacheSizeMax ]
	do
		for((i=0; i<PolicyCount; i++))
		do
			echo "Policy=${Policy[i]}, CacheSize=$CacheSize, trace=$TRACEDIR/$tracefile"
			$PROGRAM -s $CacheSize -i90000000 -p ${Policy[i]} $TRACEDIR/$tracefile -l1000 >> ${Policy[i]}.log  
		done

		((CacheSize *= 2))
	done

done < $1
