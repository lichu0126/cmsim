#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../sim

#./test.sh trace.list 

Policy=(min)
PolicyCount=${#Policy[@]}
while read tracefile
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi
	CacheSize=$CacheSizeMin
	while [ $CacheSize -le $CacheSizeMax ]
	do
		for((i=0; i<PolicyCount; i++))
		do
			echo "Policy=${Policy[i]}, CacheSize=$CacheSize, trace=$TRACEDIR/$tracefile"
			trace=`basename $tracefile`
			#log=${trace%%\.*}_$LINES.log
			log=${trace%%\.*}_$LINES"_"${Policy[i]}.log
			$PROGRAM -s $CacheSize -i90000000 -p ${Policy[i]} -t ascii $TRACEDIR/$tracefile -l $LINES>>	$log
			echo $log
		done

		#((CacheSize *= 2))
		((CacheSize += 1024))
	done

done < $1
