#########################################################################
# File Name: stat.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 13时03分45秒
#########################################################################
#!/bin/bash

source common.sh

row=0
gap=0

Policy=("lruwsr")
PolicyCount=${#Policy[@]}
echo ${Policy[@]} "***************hits****************"
grep "total hits" $1 |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
echo ${Policy[@]} "***************disk write**********"
grep "disk write" $1 |awk '{print $4}'|tr '\n' ' '|xargs -n $PolicyCount 

echo ${Policy[@]} "***************read hits**********"
grep "read  hits" $1 |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 

echo ${Policy[@]} "***************write hits**********"
grep "write hits" $1 |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
