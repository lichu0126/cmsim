#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../sim

#./test.sh trace.list 

while read tracefile
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi

	echo "Policy=${Policy[i]}, CacheSize=$CacheSize, trace=$TRACEDIR/$tracefile"
	trace=`basename $tracefile`
	log=${trace%%\.*}
	fulltracefile=$TRACEDIR/$tracefile
	./freper_csv_new.py $fulltracefile $log
done < $1
