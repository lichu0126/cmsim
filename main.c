/*************************************************************************
    > File Name: main.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月21日 星期六 19时38分08秒
 ************************************************************************/

#include "cache_sim.h"
#define _GNU_SOURCE
#include <getopt.h>


FILE *tracefile = NULL;
int traceformat = MSR;
char *program_name;

void usage()
{
	printf("\
Usage: %s [OPTION]... TRACEFILE\n",program_name);
	printf("\
	-h, --help		show this help file\n\
	-a, --alpha		the alpha for zipf/pareto\n\
	-b, --blocksize[Bytes]	cache block size, should be power of 2\n\
	-s, --cachesize		number of cache blocks\n\
	-p, --policy		the name of the policy\n\
	-t, --type		the type of trace (from file, or synthetic)\n\
	-w, --workset[MB]	the working set for synthetic trace\n\
	-i, --intval		the interval for cache stats\n\
	-l, --lines		the lines of traces to read or gernerate\n\
	-r, --readratio		read ratio for synthetic trace[default:%.1f]\n\
	-m, --mode		0:WB, 1:RO, 2:WT\n\
	-A, --Architecture	0:ssd+backstore[default], 1:backstore, 2:ssd+hdd+backstore\n\
	-R, --Realtest		0:only for computing hit ratios[default],\n\
				1:real read/write and write dirty blocks to HDD\n\
				2:real read/write and write all cache blocks to HDD\n\
	-L, --Logshow		0: Do not show the log details [default]\n\
				1: Log dirty blocks to HDD \n\
				2: Log all cache blocks to HDD\n",SYNTH_READRATIO);
}

void parse(int argc, char **argv, cache_manager *cm){
	int opt;
	int dist_val_set = 0;
	struct option longopts[] = {
		{"blocksize",1,NULL,'b'},
		{"cachesize",1,NULL,'s'},
		{"policy",1,NULL,'p'},
		{"tracetype",1,NULL,'t'},
		{"mode",1,NULL,'m' },
		{"intval",1,NULL,'i' },
		{"workset",1,NULL,'w' },
		{"lines",1,NULL,'l' },
		{"alpha",1,NULL,'a' },
		{"readratio",1,NULL,'r'},
		{"Architecture",1,NULL,'A'},
		{"Realtest",1,NULL,'R'},
		{"Logshow",1,NULL,'L'},
		{GETOPT_HELP_OPTION_DECL},
		{0,0,0,0}};

	while ((opt = getopt_long(argc,argv, ":b:r:s:p:t:m:hi:w:l:a:R:A:L:", longopts, NULL)) != -1 ) {
		switch (opt) {
			case 'b':
				cm->cm_bsize = atoi(optarg);
				if (((cm->cm_bsize) & (cm->cm_bsize - 1))!= 0){
					fprintf(stderr, "the cache block size should be power of 2!\n");
					exit(-1);
				}
				break;
			case 's':
				cm->cm_size = atoi(optarg);
				break;
			case 'p':
				strcpy(cm->policy_name,optarg);
				break;
			case 'm':
				cm->cm_mode = atoi(optarg);
				break;
			case 't':
				strcpy(cm->tracetype, optarg);
				if(strcmp(optarg, "ascii")  == 0)
					traceformat = ASCII;
				else if (strcmp(optarg, "msr") == 0)
					traceformat = MSR;
				else if (strcmp(optarg,"zipf") == 0)
					traceformat = SYNTH_ZIPF;
				else if (strcmp(optarg,"pareto") == 0)
					traceformat = SYNTH_PARETO;
				else
					traceformat = -1;
				break;
			case 'r':
				cm->cm_synth_read_ratio = atof(optarg);
				break;
			case 'a':
				cm->cm_alpha = atof(optarg);
				dist_val_set = 1;
				break;
			case 'i':
				cm->cm_stat_intval = atoi(optarg);
				break;
			case 'w':
				cm->cm_working_set = atoi(optarg);
				printf("set working set to %d\n", cm->cm_working_set);
				break;
			case 'l':
				cm->cm_trace_lines = atoi(optarg);
				break;
			case 'A':
				cm->arch = atoi(optarg);
				break;
			case 'R':
				cm->realtest = atoi(optarg);
				printf("set realtest to %d\n",cm->realtest);
				break;
			case 'L':
				cm->logshow = atoi(optarg);
				break;
			case 'h':
				usage();
				exit(-1);
				break;
			case ':':
				printf("option needs a value\n");
				break;
			case '?':
				printf("Unknown option: %c\n",optopt);
				break;
		}
	}
//	printf("argc=%d, optind=%d\n",argc,optind);
	cm->traceformat = traceformat;
//	SYNTH_ZIPF is the first synthetic trace 
	if ( traceformat < SYNTH_ZIPF ) {	/*It's not a synthetic trace'*/
		if( argc-1 < optind ){
			fprintf(stderr, "miss the trace file\n");
			usage();
			exit(-1);
		}
	//	printf("the tracefile is %s\n",argv[optind]);
		if( (tracefile = fopen(argv[optind],"r")) == NULL){
			fprintf(stderr, "open file %s error!\n", argv[optind]);
			exit(-1);
		}
		cm->tracefile = tracefile;
		strcpy(cm->tracefilename, argv[optind]);
	}
	else {		/*for synthetic trace*/
		/* Use cm_bsize as unit, when generate LBA, 
		 * it should be multiped by (cm_bsize/SECTOR_SIZE)*/
		unsigned long nranges = (WORKSET_UNIT/cm->cm_bsize) * cm->cm_working_set;
		//printf("nranges = %lu\n",nranges);
		strcpy(cm->tracefilename, "Synthetic");
		if(traceformat == SYNTH_ZIPF) {
			if(!dist_val_set)
				cm->cm_alpha = DEF_ZIPF_VAL;
			else if(cm->cm_alpha == 1.0) {
				printf("zipf input must be different than 1.0\n");
				exit(-1);
			}
			zipf_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
		else if(traceformat == SYNTH_PARETO) {
			if (!dist_val_set)
				cm->cm_alpha = DEF_PARETO_VAL;
			else if (cm->cm_alpha >= 1.00 || cm->cm_alpha < 0.00) {
				printf("pareto input must be > 0.00 and < 1.00\n");
				exit(-1);
			}
			pareto_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
	}
}


static inline double now(void)                                                  
{
    struct timeval tv; 
    gettimeofday(&tv, 0); 
    return tv.tv_sec + 1e-6*tv.tv_usec;
}



int main(int argc, char **argv)
{
	int reqcount = 0;
	ioreq_event *new=NULL;
	cache_manager *cm=NULL;
	program_name = (char *) argv[0];
	if (argc < 2){
		printf("args are missed\n");
		printf("sizeof off_t is %d\n",sizeof(off_t));
		usage();
		return -1;
	}
	cm = cm_init(cm);
	parse(argc,argv,cm);
	//traceformat = cm->traceformat;
	show_cache_config(cm);

	cm_setup(cm);

	new = malloc(sizeof(ioreq_event));
	assert(new!=NULL);

	if(strcmp(cm->policy_name,"min") == 0 || \
			strcmp(cm->policy_name, "mins") == 0 ) {

		while ((new=iotrace_get_ioreq_event(cm,traceformat,new))!= NULL ){
			reqcount++;
			if(strcmp(cm->policy_name,"min") == 0 )
				min_scan_trace(cm, new);
			else if(strcmp(cm->policy_name,"mins") == 0 )
				mins_scan_trace(cm, new);
			if (reqcount >= cm->cm_trace_lines)
				break;
		}

		/*Now we just turn everything back*/
		reqcount = 0;
		unsigned long nranges = (WORKSET_UNIT/cm->cm_bsize) * cm->cm_working_set;
		if(traceformat < SYNTH_ZIPF) 
			fseek(cm->tracefile,0,SEEK_SET);
		else if(traceformat == SYNTH_ZIPF) {
			zipf_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
		else if(traceformat == SYNTH_PARETO) {
			pareto_init(&cm->cm_zs,nranges,cm->cm_alpha, 1);
		}
	}

	new = malloc(sizeof(ioreq_event));
	
	double begin,end;
	begin = now();
	while ((new=iotrace_get_ioreq_event(cm,traceformat,new))!= NULL ){
		reqcount++;
//		new->flags=0;	/*for dbg*/
		cm_access(cm, new);
		if (reqcount >= cm->cm_trace_lines)
			break;
	}
	end = now();
	
	printf("*****The final cache stats******\n");
	cm_print_stat(cm);

	//printf("latency is %.3f s\n",cm->stat_curr-cm->stat_last);
	printf("*****");
	printf("total latency is %.3f s\n",end-begin);
	if(traceformat < SYNTH_ZIPF) fclose(tracefile);

	cache_exit(cm);
	return 1;
}
