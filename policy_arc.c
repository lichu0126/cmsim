/*************************************************************************
    > File Name: policy_arc.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: Wed Jun 25 10:19:31 2014
 ************************************************************************/

/*
 * Referemce:
 * FAST'03 ARC: A Self-Tuning, Low Overhead Replacement Cache
 * */

#include "cache_sim.h"

#define MAX_QUEUE	4
#define T1	0
#define T2	1
#define B1	2
#define B2	3
//#define NA	100	/*N/A means not in any list*/

/*MRU->head, LRU->tail*/
static struct list_head arc_list[MAX_QUEUE] = {
	LIST_HEAD_INIT( arc_list [0] ),
	LIST_HEAD_INIT( arc_list [1] ),
	LIST_HEAD_INIT( arc_list [2] ),
	LIST_HEAD_INIT( arc_list [3] ),
};
static int arc_len[MAX_QUEUE] = {0};
static int p = 0;

#define T1_LEN	arc_len[T1]
#define T2_LEN	arc_len[T2]
#define B1_LEN	arc_len[B1]
#define B2_LEN	arc_len[B2]
/* Move cb to MRU in arc_list[idx], 
 * if idx == NA, it means moving to free_list*/
static void move_to_mru (struct cache_block *cb, int idx ) 
{
	struct list_head *mru;
	struct cache_manager *cm = cb->cb_cm; 
	int idx_s = cb->cb_list_idx;

	dprintf("move from %d to %d %u\n",idx_s, idx, cb->cb_blkno * 512);

	list_del_init(&cb->cb_list);

	if ( idx == NA ) {
		mru = &cm->cm_free_list;
		cm_remove_hash(cb);

	}
	else {
		mru = &arc_list[idx];
		arc_len[idx] ++;
	}

	if(idx_s != NA )
		arc_len[idx_s]--;
	list_add(&cb->cb_list, mru);
	cb->cb_list_idx = idx;

	if(idx == T1 || idx == T2)
		set_bit(CB_RESIDENT, &cb->cb_flags);
	else
		clear_bit(CB_RESIDENT, &cb->cb_flags);


	/* Moving cb from T1 or T2 to B1/B2 and freelist.
	 * We only minus 1 here because the DIRTY flag may 
	 * be chenged later, and the ++ operation should be 
	 * happening outside.
	 * */
	if(idx_s <= T2 && idx > T2 ) {	
		test_and_clear_bit(CB_DIRTY, &cb->cb_flags) ? \
			cm_flush_block(cb) :	\
			cm->cm_clean_count--;
	}
}

/* idx_s: source list idx for removing  lru 
 * idx_t: target list idx for inserting mru
 * if idx_t == NA, it means free that block
 * */
static void move_lru2mru(int idx_s, int idx_t)
{
	struct cache_block *cb;
	struct list_head *lru;
	struct cache_manager *cm;

	assert(!list_empty(&arc_list[idx_s]));
	lru = arc_list[idx_s].prev;
	cb = list_entry (lru, struct cache_block, cb_list);
	cm = cb->cb_cm;
	
	move_to_mru(cb, idx_t);
}

/*idx represents the request's list index*/
static void cache_replace(int idx, int p)
{
	if ( (arc_len[T1]!=0) && ( (arc_len[T1] > p) || (idx==B2 && arc_len[T1]==p))) {
		move_lru2mru(T1, B1);
	}
	else {
		move_lru2mru(T2, B2);
	}

}

static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	struct cache_block *cb;
	int list_idx, delta;
	int c = cm->cm_size;

	dprintf("|T1|=%d |T2|=%d |B1|=%d |B2|=%d\n",T1_LEN, T2_LEN, B1_LEN, B2_LEN);
	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		list_idx = cb->cb_list_idx;
		dprintf("cache read hit int list %d, lba=%d\n",list_idx,lba);
		/*CASE 1: in T1 or T2. A cache hit in ARC(C)*/
		if( list_idx == T1 || list_idx == T2) {
			/*Move cb to MRU of T2 list*/
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			move_to_mru(cb, T2);
			cm->cm_write_hits++;
		}

		/*CASE 2: hit in B1. A cache miss in ARC(C)*/
		else if( list_idx == B1 ) {
			delta = (arc_len[B1] >= arc_len[B2])? 1 : arc_len[B2]/arc_len[B1];
			p = min(p+delta,c);

			cache_replace(cb->cb_list_idx, p);
			/*Move cb from B1 to MRU of T2, and fetch teh data*/	
//			cm_disk_read(cm);
			if( fetch )
				cm_fetch_block(cb, lba);
			cm_write_block(cb);
			move_to_mru(cb, T2);
		}
		
		/*CASE 3: hit in B2. A cache miss in ARC(c)*/
		else if ( list_idx == B2 ) {
			delta = (arc_len[B2] >= arc_len[B1])? 1 : arc_len[B1]/arc_len[B2];
			p = max(p-delta,0);

			cache_replace(cb->cb_list_idx, p);
			/*Move cb from B1 to MRU of T2, and fetch teh data*/	
			//cm_disk_read(cm);
			if ( fetch )
				cm_fetch_block(cb, lba);
			cm_write_block(cb);
			move_to_mru(cb, T2);
		}
	}

	/*CASE 4: A cache miss in ARC(C) and DBL(2c)*/
	else {
		/*CASE A: L1=T1+B1 has exactly c pages*/
		if ( arc_len[T1] + arc_len[B1] == c ) {
			if ( arc_len[T1] < c ) {
				/*Delete LRU page in B1*/
				assert(!list_empty(&arc_list[B1]));
				move_lru2mru(B1, NA);
				cache_replace(NA, p);
			}
			else {
				/*B1 is empty. Delete LRU page in T1*/
				assert(list_empty(&arc_list[B1]));
				move_lru2mru(T1, NA);
			}
		}
		else if ( arc_len[T1] + arc_len[B1] < c ) {
			int i,arc_len_sum ;
			for (i = 0, arc_len_sum = 0; i < MAX_QUEUE ; i++ )
				arc_len_sum+=arc_len[i];
			if ( arc_len_sum >= c ) {
				if ( arc_len_sum == c*2 ) {
					//Delete LRU page in B2
					move_lru2mru(B2, NA);
				}
				cache_replace(NA, p);
			}
		}

		/*Now, fetch data to cache and move to MRU in T1*/
		dprintf("write cache miss lba=%d\n",lba);
		if ( (cb = cm_get_free_block(cm)) == NULL)
			cb = cm_alloc_block(cm);
		cb = cm_reset_block(cm, cb);
		cb->cb_blkno = lba;
		if ( fetch )
			cm_fetch_block(cb, lba);
		cm_write_block(cb);
		move_to_mru(cb, T1);
		cm_insert_hash(cm,cb);
		
	}
	return 1;
}


static int cache_read(struct cache_manager *cm, int lba)
{
	struct cache_block *cb;
	int list_idx, delta;
	int c = cm->cm_size;

	dprintf("|T1|=%d |T2|=%d |B1|=%d |B2|=%d\n",T1_LEN, T2_LEN, B1_LEN, B2_LEN);
	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		list_idx = cb->cb_list_idx;
		dprintf("cache read hit int list %d, lba=%d\n",list_idx,lba);
		/*CASE 1: in T1 or T2. A cache hit in ARC(C)*/
		if( list_idx == T1 || list_idx == T2) {
			/*Move cb to MRU of T2 list*/
			move_to_mru(cb, T2);
			cm->cm_read_hits++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}

		/*CASE 2: hit in B1. A cache miss in ARC(C)*/
		else if( list_idx == B1 ) {
			delta = (arc_len[B1] >= arc_len[B2])? 1 : arc_len[B2]/arc_len[B1];
			p = min(p+delta,c);

			cache_replace(cb->cb_list_idx, p);
			/*Move cb from B1 to MRU of T2, and fetch teh data*/	
//			cm_disk_read(cm);
			cm_fetch_block(cb, lba);
			move_to_mru(cb, T2);
		}
		
		/*CASE 3: hit in B2. A cache miss in ARC(c)*/
		else if ( list_idx == B2 ) {
			delta = (arc_len[B2] >= arc_len[B1])? 1 : arc_len[B1]/arc_len[B2];
			p = max(p-delta,0);

			cache_replace(cb->cb_list_idx, p);
			/*Move cb from B1 to MRU of T2, and fetch teh data*/	
			//cm_disk_read(cm);
			cm_fetch_block(cb, lba);
			move_to_mru(cb, T2);
		}
	}

	/*CASE 4: A cache miss in ARC(C) and DBL(2c)*/
	else {
		/*CASE A: L1=T1+B1 has exactly c pages*/
		if ( arc_len[T1] + arc_len[B1] == c ) {
			if ( arc_len[T1] < c ) {
				/*Delete LRU page in B1*/
				assert(!list_empty(&arc_list[B1]));
				move_lru2mru(B1, NA);
				cache_replace(NA, p);
			}
			else {
				/*B1 is empty. Delete LRU page in T1*/
				assert(list_empty(&arc_list[B1]));
				move_lru2mru(T1, NA);
			}
		}
		else if ( arc_len[T1] + arc_len[B1] < c ) {
			int i,arc_len_sum ;
			for (i = 0, arc_len_sum = 0; i < MAX_QUEUE ; i++ )
				arc_len_sum+=arc_len[i];
			if ( arc_len_sum >= c ) {
				if ( arc_len_sum == c*2 ) {
					//Delete LRU page in B2
					move_lru2mru(B2, NA);
				}
				cache_replace(NA, p);
			}
		}

		/*Now, fetch data to cache and move to MRU in T1*/
		dprintf("read cache miss lba=%d\n",lba);
		if ( (cb = cm_get_free_block(cm)) == NULL)
			cb = cm_alloc_block(cm);
		cb = cm_reset_block(cm, cb);
		cb->cb_blkno = lba;
//		cm_disk_read(cm);
		cm_fetch_block(cb, lba);
		move_to_mru(cb, T1);
		cm_insert_hash(cm,cb);
		
	}
	return 1;
}





struct cm_personality arc_personality = 
{
	.policy_name = "arc",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int arc_register(cache_manager *cm)
{
	cm_personality_register(cm, &arc_personality);
	return 0;
}

