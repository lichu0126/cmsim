CC=/usr/bin/gcc
#getconf LONG_BIT to get the BITS_PER_LONG
										#-march is for __sync_add_and_fetch 
CFLAGS=-g -Wall -O3 -DBITS_PER_LONG=32  -march=i686				 #for 32Bit OS
#CFLAGS=-g -Wall -O3 -DBITS_PER_LONG=32  -march=x86-64           #for 64bit OS
#CFLAGS=-g -Wall -DDBG
LDFLAGS=
LIBS=-lm -lpthread
POLICIES=$(wildcard policy_*.c)
#SOURCES= cache_sim.c cm_iotrace.c log.c policy_arc.c policy_ccflru.c policy_cflru.c policy_fifo.c policy_harc.c policy_lru.c policy_lruwsr.c policy_min.c rand.c zipf.c policy_plfu.c
SOURCES= cache_sim.c cm_iotrace.c log.c rand.c zipf.c $(POLICIES)
HEADERS=$(wildcard *.h)
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=sim
TRACESTAT=tracestat

#all: $(SOURCES) $(EXECUTABLE)
all: $(EXECUTABLE)	 $(TRACESTAT)

$(EXECUTABLE): $(OBJECTS) $(HEADERS) main.o
	$(CC) $(LDFLAGS) $(OBJECTS) main.o $(LIBS) -o $@

$(TRACESTAT): $(OBJECTS) $(HEADERS) tracestat.o
	$(CC) $(LDFLAGS) $(OBJECTS) tracestat.o $(LIBS) -o $@

install:
	/bin/cp $(EXECUTABLE) /usr/local/bin/

.PHONY: clean
clean:
	rm -f *.o ${EXECUTABLE}
