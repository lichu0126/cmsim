/*************************************************************************
    > File Name: policy_harc.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月26日 星期四 14时17分12秒
 ************************************************************************/

/*
 * Referemce:
 * MSST'14 H-ARC: A Non-Volatile Memory Based Cache Policy for Solid State Drives
 * */

#include "cache_sim.h"


#define MAX_QUEUE	8
#define CT1	0	/*Clean Region Top, C1i in the paper*/
#define CT2	1	/*C2i in the paper*/
#define DT1 2	/*D1i in the paper*/
#define DT2 3	/*D2i in the paper*/
#define CB1	4	/*C1o in the paper*/
#define CB2	5	/*C2o in the paper*/
#define DB1 6	/*D1o*/
#define DB2 7	/*D2o*/
//#define NA	100	/*N/A means not in any list*/

#define WRITE_MULTIPLIER 2	/*to enlarge DT faster than CT*/

/*MRU->head, LRU->tail*/
static struct list_head arc_list[MAX_QUEUE] = {
	LIST_HEAD_INIT( arc_list [0] ),
	LIST_HEAD_INIT( arc_list [1] ),
	LIST_HEAD_INIT( arc_list [2] ),
	LIST_HEAD_INIT( arc_list [3] ),
	LIST_HEAD_INIT( arc_list [4] ),
	LIST_HEAD_INIT( arc_list [5] ),
	LIST_HEAD_INIT( arc_list [6] ),
	LIST_HEAD_INIT( arc_list [7] ),
};
static int arc_len[MAX_QUEUE] = {0};
static int p = 0;	/*target real pages for CT1+CT2*/
static int pc = 0;	/*target real pages for CT1*/
static int pd = 0;	/*... for DT1*/


#define CB_LEN  (arc_len[CB1]+arc_len[CB2])
#define CT_LEN  (arc_len[CT1]+arc_len[CT2])
#define DB_LEN  (arc_len[DB1]+arc_len[DB2])
#define DT_LEN  (arc_len[DT1]+arc_len[DT2])
#define C1_LEN	(arc_len[CT1]+arc_len[CB1])
#define D1_LEN	(arc_len[DT1]+arc_len[DB1])

#define SUM_LEN	(CB_LEN+CT_LEN+DT_LEN+DB_LEN)	/*real+ghost blocks*/
/* Move cb to MRU in arc_list[idx], 
 * if idx == NA, it means moving to free_list*/
static void move_to_mru (struct cache_block *cb, int idx ) 
{
	struct list_head *mru;
	struct cache_manager *cm = cb->cb_cm; 
	int idx_s = cb->cb_list_idx;

	dprintf("move from %d to %d %lu\n",idx_s, idx, cb->cb_blkno * 512);

	list_del_init(&cb->cb_list);

	if ( idx == NA ) {
		mru = &cm->cm_free_list;
		cm_remove_hash(cb);

	}
	else {
		mru = &arc_list[idx];
		arc_len[idx] ++;
	}

	if(idx_s != NA )
		arc_len[idx_s]--;
	list_add(&cb->cb_list, mru);
	cb->cb_list_idx = idx;

	if( idx <= DT2 )
		set_bit(CB_RESIDENT, &cb->cb_flags);
	else
		clear_bit(CB_RESIDENT, &cb->cb_flags);


	/* Moving cb from T1 or T2 to B1/B2 and freelist.
	 * We only minus 1 here because the DIRTY flag may 
	 * be chenged later, and the ++ operation should be 
	 * happening outside.
	 * */
	if(idx_s <= DT2 && idx > DT2 ) {	
		test_and_clear_bit(CB_DIRTY, &cb->cb_flags) ? \
			cm_flush_block(cb) :	\
			cm->cm_clean_count--;
	}
}

/* idx_s: source list idx for removing  lru 
 * idx_t: target list idx for inserting mru
 * if idx_t == NA, it means free that block
 * */
static void move_lru2mru(int idx_s, int idx_t)
{
	struct cache_block *cb;
	struct list_head *lru;
	
	assert (!list_empty(&arc_list[idx_s]));
	lru = arc_list[idx_s].prev;
	cb = list_entry (lru, struct cache_block, cb_list);
	assert(cb->cb_list_idx == idx_s);
	
	move_to_mru(cb, idx_t);
}

/* Eviction&Balance algorithm in the paper */
/* Make sure one real page are removed */
static void cache_replace(int idx, int p)
{
	dprintf("CT1=%d CT2=%d DT1=%d DT2=%d\n",arc_len[CT1],arc_len[CT2] \
			,arc_len[DT1],arc_len[DT2]);
	if ( CT_LEN > p || ( CT_LEN==p && (idx==DB1||idx==DB2)) )  {
		/*evict a page from CT*/
		if ( arc_len[CT1] > pc ) {
			move_lru2mru(CT1, CB1);
			return;
		}
		else if ( arc_len[CT2] > 0)	{
			move_lru2mru(CT2, CB2);
			return;
		}

	}

	/*evict a page from DT*/
	if ( arc_len[DT1] > pd )
		move_lru2mru(DT1, DB1);
	else  if ( arc_len[DT2] > 0)
		move_lru2mru(DT2, DB2);
	else {	
		/* if it comes here, that means |DT2|==0
		 * The original paper didn't describe what to do
		 * I'll just choose the largest one to replace.*/
		if( arc_len[CT1] > arc_len[CT2])
			move_lru2mru(CT1, CB1);
		else if ( arc_len[CT2] > 0 )
			move_lru2mru(CT2, CB2);
		else 
			move_lru2mru(DT1, DB1);
	}

}






static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;
	int list_idx, delta;
	int c = cm->cm_size;

	dprintf("op=%d lba=%d CT_LEN=%d DT_LEN=%d CB_LEN=%d DB_LEN=%d\n",op,lba, CT_LEN, DT_LEN,CB_LEN, DB_LEN);
//	printf(" p=%d pc=%d pd=%d\n",p,pc,pd);
	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		list_idx = cb->cb_list_idx;
		dprintf("op=%d hit in list %d, lba=%d CT_LEN=%d DT_LEN=%d CB_LEN=%d DB_LEN=%d\n",op, list_idx,lba, CT_LEN, DT_LEN,CB_LEN, DB_LEN);
		
		/*CASE 1: Real Cache Hit*/
		if ( list_idx <= DT2 ) {
			if( op == READ ) {
				cm->cm_read_hits ++;
				if(test_bit(CB_DIRTY, &cb->cb_flags))
					cm->cm_dirty_read_hits++;
				if ( list_idx == CT1 || list_idx == CT2 ) 
					move_to_mru( cb, CT2 );
				else 
					move_to_mru( cb, DT2 );
			}
			else {
				if(test_bit(CB_DIRTY, &cb->cb_flags))        
					cm->cm_dirty_write_hits++;
				cm_write_block(cb);
				move_to_mru( cb, DT2 );
				cm->cm_write_hits++;
			}
		}

		/*CASE 2: Real cache miss, Ghost cache hit*/
		else if ( list_idx < MAX_QUEUE) {
			/*hit in CB1 or CB2, enlarge p and adjust pc*/
			if ( list_idx <= CB2 ) {
				p = min (p + 1, c);
				if ( list_idx == CB1 ) {
					delta = ( arc_len[CB2] < arc_len[CB1]) ? 1 : \
							arc_len[CB2]/arc_len[CB1];
					pc = min (pc + delta, p);
				}
				else {
					delta = ( arc_len [CB1] < arc_len[CB2] ) ? 1 : \
							arc_len [CB1]/arc_len[CB2];
					pc = max (pc - delta, 0);
				}
			}

			/*hit in DB1 or DB2, decrease p and adjust pd*/
			else {
				dprintf("list_idx=%d CB_LEN=%d DB_LEN=%d\n",list_idx,CB_LEN, DB_LEN);
				delta = ( CB_LEN < DB_LEN )? WRITE_MULTIPLIER :  \
						(int)(CB_LEN/DB_LEN) * WRITE_MULTIPLIER  ;
				p = max (p - delta , 0);

				if ( list_idx == DB1 ) {
					delta = ( arc_len[DB2] < arc_len[DB1]) ? 1 : \
							arc_len[DB2]/arc_len[DB1];
					pd = min (pd + delta, c-p);
				}
				else if ( list_idx == DB2 ){
					delta = ( arc_len [DB1] < arc_len[DB2] ) ? 1 : \
							arc_len [DB1]/arc_len[DB2];
					pd = max (pd - delta, 0);
				}
			}
			assert( CT_LEN + DT_LEN == c );	/*cache must be full*/
			cache_replace(cb->cb_list_idx, p);
			if ( op == READ ) {
				cm_fetch_block(cb, lba);
				move_to_mru(cb, CT2);
			}
			else {
				if ( fetch )
					cm_fetch_block(cb, lba);
				cm_write_block(cb);
				move_to_mru(cb, DT2);
			}
			assert( CT_LEN + DT_LEN == c );	/*cache must be full*/
		}// end of CASE 2 

		/*CASE 3: Miss in both real and ghost cache*/
	}
	else {
		dprintf("miss all lba=%d op=%d C1_LEN=%d D1_LEN=%d\n",lba, op,C1_LEN,D1_LEN);
		if ( CT_LEN + DT_LEN >= c ) {
			int evict_idx = -1;
			if ( CT_LEN + CB_LEN > c ) {
				if( C1_LEN > c/2 && arc_len[CB1] )
					evict_idx = CB1;
				else if ( arc_len[CB2] )
					evict_idx = CB2;
			}
			if ( evict_idx != -1 ) {
				if( D1_LEN > c/2 && arc_len[DB1])
					evict_idx = DB1;
				else if ( arc_len[DB2] )
					evict_idx = DB2;
			}

			dprintf("CB1=%d CB2=%d DB1=%d DB2=%d\n",arc_len[CB1],arc_len[CB2], \
				arc_len[DB1], arc_len[DB2]);

			if (evict_idx != -1) {
				move_lru2mru(evict_idx, NA);
			}
			else if (SUM_LEN == 2*c ) {
			/* if it comes here, that means we don't find proper 
			 * CB or DB to evict. The original paper didn't describe what to do.
			 * Like in cache_replace, I'll choose the biggest to replace while 
			 * the sum of real and ghost blocks exceeds 2*c.*/
				int i, max;
				for (i=CB1, max=0; i <=DB2; i++ ) {
					if (arc_len[i] > max ) {
						max = arc_len[i];
						evict_idx = i;
					}
				}
				move_lru2mru(evict_idx,NA);
			}/*same with */

			cache_replace(NA, p);
		}


		if ( (cb = cm_get_free_block(cm)) == NULL)
			cb = cm_alloc_block(cm);
		cb = cm_reset_block(cm, cb);
		cb->cb_blkno = lba;
		if ( op== READ ){
			cm_fetch_block(cb, lba);
			move_to_mru(cb, CT1);
			cm_insert_hash(cm,cb);
		}
		else if ( op == WRITE ){
			if ( fetch )
				cm_fetch_block(cb, lba);
			cm_write_block(cb);
			move_to_mru(cb, DT1);
			cm_insert_hash(cm,cb);
		}
	}

	return 0 ;
}



static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}

struct cm_personality harc_personality = 
{
	.policy_name = "harc",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int harc_register(cache_manager *cm) {
	cm_personality_register(cm, &harc_personality);
	return 0;
}

