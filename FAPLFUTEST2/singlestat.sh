#########################################################################
# File Name: stat.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 13时03分45秒
#########################################################################
#!/bin/bash

source common.sh

row=0
gap=0
#"***************hits****************"
LOGFILE=$1
Policy=("faplfu-128K")
PolicyCount=${#Policy[@]}

function gethits {
	echo ${Policy[@]} 
	grep "total hits" $LOGFILE |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
}
#"***************read hits**********"
function getreadhits {
	echo ${Policy[@]} 
	grep "read  hits" $LOGFILE |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
}

function getwritehits {
	echo ${Policy[@]} 
	grep "^write hits" $LOGFILE |awk '{print $5}'|tr '\n' ' '|xargs -n $PolicyCount 
}
function getdirtywritehits {
	echo ${Policy[@]} 
	grep "dirty write hits" $LOGFILE |awk '{print $6}'|tr '\n' ' '|xargs -n $PolicyCount 
}
function getdirtyreadhits {
	echo ${Policy[@]} 
	grep "dirty read hits" $LOGFILE |awk '{print $6}'|tr '\n' ' '|xargs -n $PolicyCount 
}

#echo ${Policy[@]} "***************disk read**********"
#grep "disk read" $LOGFILE |awk '{print $4}'|tr '\n' ' '|xargs -n $PolicyCount 
function getdiskwrite {
	echo ${Policy[@]} 
	grep "disk write" $LOGFILE |awk '{print $4}'|tr '\n' ' '|xargs -n $PolicyCount 
}

DATFILE=${LOGFILE%\.*}.dat 
gethits |tee hits_$DATFILE
getreadhits |tee rdhits_$DATFILE
getwritehits |tee wrhits_$DATFILE
getdirtywritehits |tee dwrhits_$DATFILE
getdirtyreadhits |tee drdhits_$DATFILE
getdiskwrite |tee diskwrites_$DATFILE

