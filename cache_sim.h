/*************************************************************************
    > File Name: cache_sim.h
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月21日 星期六 19时39分00秒
 ************************************************************************/
#ifndef __CACHE_MANAGER_H
#define __CACHE_MANAGER_H

#define _GNU_SOURCE             // for O_DIRECT
#define _FILE_OFFSET_BITS 64
//#define _LARGEFILE64_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/time.h>
#include<pthread.h>
#include<unistd.h>

#include "list.h"
#include "utils.h"
#include "zipf.h"
#include "log.h"

#define HDD_DEV		"/dev/sdb1"
#define SSD_DEV		"/dev/sdc1"
#define BACK_DEV	"/dev/sdd1"
//#define BACK_DEV	"/dev/ramdisk"

#define HDD_VALID	0	/*if set to 0, we don't write to HDD*/

#define PARALLEL_WT	0	/*if set to 1, we write cache and Backstore concurrently*/
#define GETOPT_HELP_OPTION_DECL \
	  "help", 0 , NULL, 'h' 

typedef struct log_segment_header lseg_header;




/*Flags for cache block*/
enum cache_block_flags {
	CB_DIRTY,
	CB_HOT,
	CB_OVERWRITE,
	CB_RESIDENT,	/*if data is not resident in memory, it is ghost block*/
	CB_OP,	/*OP from user application READ:1 WRITE:0 */
};

#define HASH_NUM (1024*1024*4)	/*should be power of 2*/
#define HASH_MASK (HASH_NUM - 1)
#define NA	100	/*N/A means not in any list*/

struct cache_block {
	struct hlist_node cb_hash;
	struct list_head cb_list;
	
	off_t cb_blkno;	/*The LBA of the Req*/
	int cb_devno;	/*Maybe used for multi-devices*/
	unsigned int cb_flags;	/*flag bits*/ 
	int cb_list_idx;	/*indicate which list it's in'*/
//	int cb_isghost;	/*For some policy, it maybe just a ghost block without data*/
	struct cache_manager *cb_cm;	/*point to the mm it belongs to*/

	int cb_next_pos; /*next reference of this block, for OPT(MIN)*/
	double cb_freq;	/*frequency of this cache block*/

	/*add by lichu@2014-07-17*/
	int cb_atomic_count;	/*for Real Read/Write as a sync counter*/
	off_t cb_ssd_off;	/*The LBA of the cache block in SSD*/

	/*for Log*/
	int cb_seg_id;	/*segment id*/
	int cb_slot_id;	/*slot id in the segment*/

	struct flash_block *cb_fb;
};

/*For FAB BPLRU ...
 * cache_block is a page, contained in flash_block
 * */
#define	PAGENUM_PER_FBLOCK	128	/*arg or define? fix later*/
struct flash_block {
	struct list_head fb_list;
	struct cache_block **fb_pages;
	int fb_page_counter;
};




/*Super Bigger Structrue than it should be...*/
typedef struct cache_manager{
	struct cm_personality *pers;
	struct list_head cm_free_list;	/*free cache block list pool*/
	struct hlist_head *cm_hashtbl;
	char policy_name[20];
	char tracetype[20];	/*just for config show...*/
	int traceformat;	/*for different type of traces*/
	char tracefilename[1024];
	FILE *tracefile;	/*The trace file*/

	struct zipf_state	cm_zs;	/*for synthetic trace [zipf/pareto]*/
	double cm_synth_read_ratio;	/*read ratio for synth trace*/
	double cm_alpha;	/*for zipf & pareto*/
	int cm_working_set;	/*working set for synthetic trace[Bytes]*/
	int cm_trace_lines;	/*trace lines to read or generate*/

	/*Cache Configuration*/
	int cm_bsize;	/*cache block size [Bytes], should be 2^x default 4K*/
	int cm_size;		/*total num of blocks in the cache*/
	int cm_mode;	/*cache mode: wt,ro,wa*/
	int cm_stat_intval;	/*intval for print cache stats*/

	/*For cache stats*/
	int cm_dirty_count;	/*num of dirty blocks in the cache*/
	int cm_clean_count; /*num of clean blocks in the cache*/
	int cm_read_hits;	/*num of read hits*/
	int cm_dirty_read_hits;	/*num of read hits on dirty block*/
	int cm_write_hits;	/*num of write hits*/
	int cm_dirty_write_hits;	/*num of write hits on dirty block*/
	int cm_disk_reads;	/*num of read requests to disk*/
	int cm_disk_writes;	/*num of write requests to disk*/
	int cm_free_count;	/*free blocks in cm_free_list*/

	int cm_read_count;	/*num of read requests aligned with cache block size*/
	int cm_write_count;	/*num of write requests aligned with cache block size*/

	/*add by lichu@2014-07-17 for Real Read/Write to ssd and disk*/
	
	unsigned int arch;	/*architecture,0:ssd+backstore[default], 1:backstore, 2:ssd+hdd+backstore */
	int realtest; /*1:real read/write to devices; 0:none[defalut]*/
	int fd_ssd;			/*fd for ssd cache device*/
	int fd_hdd;			/*fd for hdd (mirror for ssd)*/
	int fd_backstore;	/*fd for back storage (iscsi tgt array)*/
	off_t ssd_size;
	off_t hdd_size;
	off_t back_size;
	void *io_buffer;		/*tmep buffer for Read/Write*/
	double stat_last;	/*for latency stats*/
	double stat_curr;
	/**/

	/*For Log*/
	int logshow;		/*whether or not show the log details*/
	lseg_header *lsegtbl;	/*log segment header table*/
	int	curr_seg_id; 
	int curr_slot_id;
	int log_tail;
#define SEGMENT_BINS	17	/*for segment valid count histogram stats, the first is only for zero bins stats*/
	int seg_bins[SEGMENT_BINS];
}cache_manager;

#define BIT_NO_SSD		0
#define BIT_ADD_HDD		1

struct cm_personality	/*api for different cache policies*/
{
	char *policy_name;	/*cache policy name*/

	void (*cache_init)(cache_manager *cm);

//	struct cache_block * (*cb_search)(int lba);
	int (*cache_read)(struct cache_manager *cm, int lba);
	int (*cache_write)(struct cache_manager *cm, int lba, int fetch);
	//int (*cache_access)(struct cache_block *cb,int lba, int fetch);
};


/*structure for iotrace, both from tracefile of synthetic*/
typedef struct ioreq_event {
	double time;
	int devno;
	off_t blkno;		/*LBA of the request block, unit is SECTOR*/
	int bcount;		/*Num of sectors(512B)*/
	int flags;		/*0:write; 1:read*/
}ioreq_event;

struct ioreq_event * iotrace_get_ioreq_event ( \
		cache_manager *cm, int traceformat, ioreq_event *temp);

#define READ 1
#define WRITE 0

#define ASCII 1
#define MSR 2

#define SYNTH_ZIPF		5
#define SYNTH_READRATIO 0.6
#define SYNTH_PARETO	6
#define DEF_PARETO_VAL	0.3
#define DEF_ZIPF_VAL	1.2



#define SECTOR_SIZE		512
#define CACHE_BSIZE		4096
#define CACHE_SIZE		8192
#define STAT_INTVAL		90000000	
#define TRACE_LINES		90000000
#define WORKING_SET		1024
#define WORKSET_UNIT	(1024*1024)	/*1MB, should be larger than cm_bsize*/

/* The mode is the same with Flashcache(facebook) and EnhanceIO*/

#define CACHE_MODE_WB	0	
#define CACHE_MODE_RO	1 /*Only write disk, invalidate cache if hit*/
#define CACHE_MODE_WT	2 /*Write both disk and cache no matter hit or miss*/


void cache_exit(cache_manager *cm);
void show_cache_config(cache_manager *cm);
void set_cache_policy(const char *name);


//void cm_invalidate(cache_manager *cm, int lba);

/*cache manager API, could be used by all cache policies, and Main()*/
cache_manager * cm_init(cache_manager *cm);
void cm_setup(cache_manager *cm);
struct cache_block * cm_alloc_block(struct cache_manager *cm);
struct cache_block * cm_get_free_block(cache_manager *cm);
struct cache_block * cm_reset_block(cache_manager *cm, struct cache_block *cb);

void cm_insert_hash(cache_manager *cm, struct cache_block *cb);
void cm_remove_hash(struct cache_block *cb);
struct cache_block * cm_search_hash(cache_manager *cm, int lba);

//int cm_read(cache_manager *cm, int lba, int len);
int cm_access(cache_manager *cm, ioreq_event *new);
int cm_disk_write(cache_manager *cm) ;
int cm_disk_read(cache_manager *cm);

int cm_fetch_block( struct cache_block *cb, int lba);
int cm_flush_block( struct cache_block *cb );
int cm_write_block ( struct cache_block *cb );

void cm_print_stat(cache_manager *cm);
void cm_reset_stat(cache_manager *cm);

void cm_personality_register(cache_manager *cm,struct cm_personality *p);

#define CACHE_READ(c,l) c->pers->cache_read(c,l)
#define CACHE_WRITE(c,l,f) c->pers->cache_write(c,l,f)



/*register for different cache policies*/
int fifo_register(cache_manager *cm) ;
int lru_register(cache_manager *cm) ;
int arc_register(cache_manager *cm) ;
int harc_register(cache_manager *cm);
int lruwsr_register(cache_manager *cm) ;
int ccflru_register(cache_manager *cm) ;
void min_scan_trace (cache_manager *cm, ioreq_event *new );
void min_print_blkstat(cache_manager *cm);
int min_register(cache_manager *cm);
int cflru_register(cache_manager *cm);
int plfu_register(cache_manager *cm);
int faplfu_register(cache_manager *cm);

int mins_register(cache_manager *cm);
void mins_scan_trace (cache_manager *cm, ioreq_event *new );

void real_write2cache(struct cache_block *cb, int nth);
void real_read_ssd(struct cache_block *cb);

/*	Add by lichu@2014-07-28
 *  Log structures (references to DCD)
 * */
#define SEGMENT_SIZE	64	/*num of slots in a segment*/
#define SEGMENT_NUM		1048576 /*num of segments, i.e. Log Size*/
struct log_segment_header {
	int		segment_id;
	int		valid_count;	/*num of valid slots*/
	off_t	slot_lba_table[SEGMENT_SIZE];
};


int cm_log_insert(struct cache_block *cb);
int cm_log_remove(struct cache_block *cb);



#endif
