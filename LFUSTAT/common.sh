#########################################################################
# File Name: common.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 13时02分21秒
#########################################################################
#!/bin/bash

TRACELIST=trace.list
TRACEDIR=/home/lichu/traces

#Policy=("lru" "arc" "harc")


Policy=(`(cd .. && ls policy*.c|sed -e 's/policy_//' -e 's/\.c//')`)
PolicyCount=${#Policy[@]}
CacheBSize=4096
CacheSizeMin=1024
CacheSizeMax=131072	#128K
LINES=4000000
#echo $PolicyCount
#echo ${Policy[@]}
