#########################################################################
# File Name: getcdf.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年09月21日 星期日 09时12分05秒
#########################################################################
#!/bin/bash

#We get cdf of the trace for LFU
#


sbt=/dev/shm/$RANDOM
sbw=/dev/shm/$RANDOM
sbr=/dev/shm/$RANDOM
wbt=/dev/shm/$RANDOM
wbw=/dev/shm/$RANDOM
wbr=/dev/shm/$RANDOM

TotalAccesses=`awk 'BEGIN{sum=0}{sum+=$4}END{print sum}' $1`
echo $TotalAccesses

#sort by total accesses
function SortByTotal {
	echo "SortByTotal"
	#sort -nrk4 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum/'$TotalAccesses'}'
	sort -nrk4 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum}'
}
function WriteByTotal {
	echo "WriteByTotal"
	sort -nrk4 $1| awk 'BEGIN{sum=0 }{sum+=$3;print sum}'
}


#sort by write accesses
function SortByWrite {
	echo "SortByWrite"
	#sort -nrk3 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum/'$TotalAccesses'}'
	sort -nrk3 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum}'
}
function WriteByWrite {
	echo "WriteByWrite"
	sort -nrk3 $1| awk 'BEGIN{sum=0 }{sum+=$3;print sum}'
}


#sort by write accesses
function SortByRead {
	echo "SortByRead"
	#sort -nrk3 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum/'$TotalAccesses'}'
	sort -nrk2 $1| awk 'BEGIN{sum=0 }{sum+=$4;print sum}'
}
function WriteByRead {
	echo "WriteByRead"
	sort -nrk2 $1| awk 'BEGIN{sum=0 }{sum+=$3;print sum}'
}


SortByTotal $1  > $sbt
SortByWrite $1  > $sbw
SortByRead	$1  > $sbr
WriteByTotal $1 > $wbt
WriteByWrite $1 > $wbw
WriteByRead  $1 > $wbr

FILE=$1
CDF=${FILE%\.*}_cdf.dat
paste $sbt $sbw $sbr $wbt $wbw $wbr  > $CDF
rm $sbt $sbw $sbr $wbt $wbw $wbr -f
