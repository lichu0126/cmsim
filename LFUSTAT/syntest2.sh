#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash


#for ZIPF 
source common.sh
Alpha=0.3	#80% access to 20% blocks
WSS=1024		#256MB
SUFIX=1m
LINES=1048576

PROGRAM=../tracestat

#./test.sh trace.list 


	log=pareto_$Alpha"_"$WSS"_""$SUFIX".log
	echo $log
	#$PROGRAM -t ascii -s $CacheSize -i90000000 -p min $TRACEDIR/$tracefile -l $LINES |sed -n '28,$p' > log
	$PROGRAM -t pareto -l $LINES -i90000000  -a $Alpha -w $WSS > $log
	sh getcdf.sh $log
	sh plotone.sh pareto_$Alpha"_"$WSS"_""$SUFIX"_cdf.dat
	display pareto_$Alpha"_"$WSS"_""$SUFIX"_cdf.png
