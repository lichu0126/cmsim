#########################################################################
# File Name: test.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年06月27日 星期五 09时40分15秒
#########################################################################
#!/bin/bash

source common.sh

PROGRAM=../tracestat

#./test.sh trace.list 

while read tracefile
do
	echo $tracefile|grep "^#"
	if [ $? -eq 0 ];then
		echo "skip this trace"
		continue
	fi
	echo "trace=$TRACEDIR/$tracefile"
	trace=`basename $tracefile`
	log=${trace%%\.*}_$LINES.log
	echo $log
	#$PROGRAM -t ascii -s $CacheSize -i90000000 -p min $TRACEDIR/$tracefile -l $LINES |sed -n '28,$p' > log
	$PROGRAM -t msr -l $LINES -i90000000 -p min $TRACEDIR/$tracefile > $log
done < $1
