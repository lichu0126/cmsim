#########################################################################
# File Name: display_zipf.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年09月21日 星期日 21时41分03秒
#########################################################################
#!/bin/bash
base=64; 
for i in `seq 1 8` 
do 
	echo $base
	display zipf_0.86_"$base"_cdf.png  &
	((base*=2))
done
