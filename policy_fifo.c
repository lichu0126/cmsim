/*************************************************************************
    > File Name: policy_fifo.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: Mon 28 Jul 2014 03:38:15 AM CST
 ************************************************************************/

#include "cache_sim.h"

static LIST_HEAD (fifo_list);
/*
 * tail --> oldest element, will be dropped
 * head --> newest element, newly added 
 * */


static struct cache_block *cache_replace( cache_manager *cm)
{
	struct list_head *tail;
	struct cache_block *cb = NULL;

	tail = fifo_list.prev;
	cb = list_entry(tail, struct cache_block, cb_list);

	list_del_init(tail);
	cm_remove_hash(cb);


	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags)) {
		cm_flush_block(cb);
	}
	else
		cm->cm_clean_count--;

	clear_bit(CB_RESIDENT, &cb->cb_flags);

	/*For Log*/
	if(cm->logshow)
		cm_log_remove(cb);

	dprintf("replce lba=%d\n",cb->cb_blkno);
	return cb;
}

static int cache_read(struct cache_manager *cm, int lba)
{
	struct cache_block *cb;		
	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {

		cm->cm_read_hits++;
		if(test_bit(CB_DIRTY, &cb->cb_flags))
			cm->cm_dirty_read_hits++;
		dprintf("cache read hit lba=%d, readhits=%d\n",lba,cm->cm_read_hits);

		if(cm->realtest)
			real_read_ssd(cb);
	}

	/*cache miss*/
	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		/*fetch data and move cb to head of fifo list*/
		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
		list_add(&cb->cb_list, &fifo_list);

//		double t=now();
//		printf("%.6f fetch begin lba=%d\n",t,lba);
		cm_fetch_block(cb, lba);
//		printf("%.6f fetch over latency is %.6f\n",now(),now()-t);

		/*only write to ssd*/
		if( cm->realtest == 1) {
				set_bit(CB_OP, &cb->cb_flags);
				real_write2cache(cb,1);
		}
		/*write to ssd and hdd concurrently*/
		else if(cm->realtest == 2) {
				set_bit(CB_OP, &cb->cb_flags);
				real_write2cache(cb,2);
		}

		/*For Log*/
		if (cm->logshow == 2)
			cm_log_insert(cb);
	}
	return 1;
}


static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	struct cache_block *cb;		

	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {

		if(test_bit(CB_DIRTY, &cb->cb_flags))                                                   	
			cm->cm_dirty_write_hits++;
		cm->cm_write_hits++;
		cm_write_block(cb);
		dprintf("cache write hit lba=%d, writehits=%d\n",lba,cm->cm_write_hits);
	}

	/*cache miss*/
	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		/*fetch data and move cb to MRU of lru_list*/
		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
		list_add(&cb->cb_list, &fifo_list);
		if(fetch)
			cm_fetch_block(cb, lba);
		cm_write_block(cb);
	}

	/*write to ssd and hdd(or backing store for PARALLEL_WT mode) concurrently*/
	if( cm->realtest ) {
		clear_bit(CB_OP, &cb->cb_flags);
		real_write2cache(cb,2);
	}

	/*For Log*/
	if( cm->logshow ) {
		cm_log_insert(cb);
	}

	return 1;
}

struct cm_personality fifo_personality = 
{
	.policy_name = "fifo",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int fifo_register(cache_manager *cm) {
	cm_personality_register(cm, &fifo_personality);
	return 0;
}
