/*************************************************************************
    > File Name: cm_iotrace.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月21日 星期六 21时21分33秒
 ************************************************************************/

#include "cache_sim.h"
static ioreq_event * iotrace_ascii_get_ioreq_event (FILE *tracefile, ioreq_event *new)
{
   char line[201];

   if (fgets(line, 200, tracefile) == NULL) {
	  dprintf("Get the end of the tracefile\n");
	  free(new);
	  return(NULL);
   }
   if (sscanf(line, "%lf %d %llu %d %x\n", &new->time, &new->devno, &new->blkno, &new->bcount, &new->flags) != 5) {
	  fprintf(stderr, "Wrong number of arguments for I/O trace event type\n");
	  fprintf(stderr, "line: %s", line);
	  exit(-1);
   }

	dprintf("%f %d % d %d %d\n", new->time,new->devno,new->blkno, new->bcount,new->flags);
   return(new);
}


static ioreq_event * iotrace_msr_get_ioreq_event (FILE *tracefile, ioreq_event *new)
{
    char line[201];
    char s1[50],s2[50];
    //static int first_line = 0;
    unsigned long long blkno;
    unsigned long long eslaped;
    //static double csv_base_time;

    if (fgets(line, 200, tracefile) == NULL) {
	  dprintf("Get the end of the tracefile\n");
	  free(new);
      return(NULL);
    }
    if (sscanf(line, "%llu,%[^,],%d,%[^,],%llu,%d,%d",&eslaped,s1,&new->devno,s2,&blkno,&new->bcount,&new->flags) != 7) {
          fprintf(stderr, "Wrong number of arguments for I/O trace event type\n");
          fprintf(stderr, "line: %s", line);
		  exit(-1);
    }
	new->time = (double)(eslaped/10000.0);
    new->blkno= blkno/512;
    new->bcount/=512;
    if (s2[0] == 'R')
        new->flags = READ;
    else
        new->flags = WRITE;

	dprintf("%f %d % d %d %d\n", new->time,new->devno,new->blkno, new->bcount,new->flags);
   return(new);
}

static ioreq_event * iotrace_synth_get_ioreq_event (cache_manager *cm, ioreq_event *new)
{
	/*The tracefile is not used now*/
	unsigned long offset;
	int cache_bsize_sectors = cm->cm_bsize/SECTOR_SIZE;

	switch (cm->traceformat) {
		case SYNTH_ZIPF:
			offset = zipf_next(&cm->cm_zs);
			break;
		case SYNTH_PARETO:
			offset = pareto_next(&cm->cm_zs);
			break;
		default:
			fprintf(stderr,"should not be here!\n");
			return NULL;
	}
	offset *= cache_bsize_sectors;

	new->devno = 0;
	new->blkno = offset;
	new->bcount = cache_bsize_sectors;
	if (drand48() < cm->cm_synth_read_ratio)
		new->flags = READ;
	else
		new->flags = WRITE;
	dprintf("%d %d %d\n", new->blkno, new->bcount,new->flags);

	return (new);
}

ioreq_event * iotrace_get_ioreq_event (cache_manager *cm, int traceformat, ioreq_event *temp){
	switch (traceformat) {
		case ASCII:
			temp = iotrace_ascii_get_ioreq_event(cm->tracefile, temp);
		    break;
		case MSR:
			temp = iotrace_msr_get_ioreq_event(cm->tracefile, temp);
		    break;

		case SYNTH_ZIPF:
		case SYNTH_PARETO:
			temp = iotrace_synth_get_ioreq_event(cm, temp);
			break;
		default:
			fprintf(stderr,"***Unknown trace format!\n");
			return NULL;

	}
	return temp;
}

