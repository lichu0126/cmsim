/*************************************************************************
    > File Name: policy_lruwsr.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月27日 星期五 18时46分52秒
 ************************************************************************/

#include "cache_sim.h"

static LIST_HEAD(lru_list);



static struct cache_block * cache_replace(cache_manager *cm)
{
	struct list_head *lru;
	struct cache_block *cb = NULL;

	while ( 1 ) {
		assert(!list_empty(&lru_list));
		lru = lru_list.prev;
		cb = list_entry (lru, struct cache_block, cb_list);
		/* If a candidate page is clean, it is selected as a 
		 * victim regardless of the status of the Cold flag*/
		if (!test_bit (CB_DIRTY, &cb->cb_flags))
			break;
		else if (!test_and_clear_bit(CB_HOT, &cb->cb_flags))
			break;
		else
			list_move(&cb->cb_list, &lru_list);
	}

	cm_remove_hash(cb);
	list_del_init(&cb->cb_list);
	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags))
		cm_flush_block (cb)	;
	else
		cm->cm_clean_count--;
	clear_bit(CB_RESIDENT, &cb->cb_flags);
	return cb;
}


static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;		

	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		dprintf("cache hit lba=%d, op=%d\n",lba,op);
		list_move(&cb->cb_list, &lru_list);	/*move to MRU*/
		set_bit(CB_HOT, &cb->cb_flags);
		if ( op == WRITE ) {
			if(test_bit(CB_DIRTY, &cb->cb_flags))        
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			cm->cm_write_hits++;
		}
		else {
			cm->cm_read_hits ++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}
	}

	/*cache miss*/
	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
		list_add(&cb->cb_list, &lru_list);
		if ( op == READ || fetch )
			cm_fetch_block(cb, lba);
		if ( op == WRITE ) 
			cm_write_block(cb);
	}
	return 1;
}


static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}



struct cm_personality lruwsr_personality = 
{
	.policy_name = "lruwsr",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int lruwsr_register(cache_manager *cm) {
	cm_personality_register(cm, &lruwsr_personality);
	return 0;
}
