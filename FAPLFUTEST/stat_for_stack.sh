#########################################################################
# File Name: stat_for_stack.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年09月19日 星期五 13时34分50秒
#########################################################################
#!/bin/bash


#arg is hits_xxx.dat
#function get_stack_hits {
#}

#arg: hits.dat  - hits2.dat
function sub_hits {
	head -n1 $1	
	paste  $1 $2|awk '{if(NR!=1) printf "%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n",$9-$1,$10-$2,$11-$3, $12-$4,$13-$5,$14-$6,$15-$7,$16-$8}'
}
function get_clean_hits_dat {
for i in rdhits*.dat
do
	sub_hits d$i $i > c$i
done

for i in wrhits*.dat
do
	sub_hits d$i $i > c$i
done
}


function get_stackhits {
	i=$1
	echo "Policy \"Clean Read Hits\" \"Dirty Read Hits\" \"Clean Write Hits\" \"Dirty Write Hits\""

	for policy_id in `seq 1 8`
	do
		policy=`cut -d' ' -f$policy_id $i| head -n1`
		#clean read hits
		crh=`cut -d' ' -f$policy_id crd$i|sed -n '2,$p'|tr '\n' ' '`
		#dirty read hits
		drh=`cut -d' ' -f$policy_id drd$i|sed -n '2,$p'|tr '\n' ' '`
		#clean write hits
		cwh=`cut -d' ' -f$policy_id cwr$i|sed -n '2,$p'|tr '\n' ' '`
		#dirty write hits
		dwh=`cut -d' ' -f$policy_id dwr$i|sed -n '2,$p'|tr '\n' ' '`
		echo $policy $crh $drh $cwh $dwh
	done
}

for datfile in hits*.dat
do
	get_stackhits $datfile > stack$datfile
done
