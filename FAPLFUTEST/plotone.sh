#########################################################################
# File Name: plot.sh
# Author: Li Chu
# mail: lichu0126@gmail.com
# Created Time: 2014年07月05日 星期六 15时34分34秒
#########################################################################
#!/bin/bash

#for FILE in hits*.dat; do
FILE=$1
PNG=${FILE%%\.*}.png
gnuplot <<- EOF
set terminal pngcairo  notransparent enhanced font "arial,10" fontscale 1.0 size 1000, 700 
set output "${PNG}"
set key inside left top vertical Right noreverse noenhanced autotitles nobox
set datafile missing '-'

set style data linespoints

#set style data histograms 
#set style histogram cluster gap 1
#set style fill solid border -1
#set boxwidth 0.9

set xtics border in scale 1,0.5 nomirror offset character 0, 0, 0 autojustify
set xtics  norangelimit font ",8"
set xtics   ("1K" 0, "2K" 1, "4K" 2, "8K" 3, "16K" 4, "32K" 5, "64K" 6, "128K" 7)
set xlabel "Cache Size (pages)"
set ylabel "Cache Hits"
#set title "Cache Hits Graph" 
i = 8
#plot 'hits.dat' using 2:xtic(1) title columnheader(2), for [i=3:8] 'hits.dat' using i title columnheader(i)
plot for [i=1:10] "${FILE}" using i title columnheader(i)
EOF
