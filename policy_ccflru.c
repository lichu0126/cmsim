/*************************************************************************
    > File Name: policy_ccflru.c
    > Author: Li Chu
    > Mail: lichu0126@gmail.com 
    > Created Time: 2014年06月27日 星期五 19时42分19秒
 ************************************************************************/

#include "cache_sim.h"

#define MAX_QUEUE	2

/*MRU->head, LRU->tail*/
static struct list_head lru_list[MAX_QUEUE] = {
	LIST_HEAD_INIT( lru_list [0] ),	/*Cold Clean LRU list*/
	LIST_HEAD_INIT( lru_list [1] ),	/*Mixed LRU List*/
};

static int lru_len[MAX_QUEUE] = {0};

#define CCL		0
#define MIX		1
#define CLEN	lru_len[CCL]
#define MLEN	lru_len[MIX]


static void move_to_mru(struct cache_block *cb, int idx )
{
	int idx_s = cb ->cb_list_idx;

	list_move( &cb->cb_list, &lru_list[idx] );
	cb->cb_list_idx = idx;

	if ( idx_s != idx ) {
		if (idx_s != NA )
			lru_len[idx_s]--;
		lru_len[idx]++;
	}

}

static struct cache_block * cache_replace(cache_manager *cm)
{
	struct list_head *lru;
	struct cache_block *cb = NULL;

	while ( 1 ) {
		if ( CLEN )	{
			lru = lru_list[CCL].prev;
			cb = list_entry (lru, struct cache_block, cb_list);
			break;
		}
		else {
			int i, loop = MLEN;
			for (i = 0; i < loop; i++ ) {
				lru = lru_list[MIX].prev;
				cb = list_entry(lru, struct cache_block, cb_list);

				if ( !test_bit( CB_HOT, &cb->cb_flags) ) {
					goto out;
				}
				else if ( !test_bit( CB_DIRTY, &cb->cb_flags)) {
					clear_bit ( CB_HOT, &cb->cb_flags);
					move_to_mru( cb, CCL);
				}
				else {
					clear_bit ( CB_HOT, &cb->cb_flags);
					move_to_mru( cb, MIX);
				}
			
			}
		}
	}

out:
	/* we don't list_del_init cb->cb_list, because it will 
	 * be done in move_to_mru later*/
	cm_remove_hash(cb);
	if(test_and_clear_bit(CB_DIRTY,&cb->cb_flags))
		cm_flush_block (cb)	;
	else
		cm->cm_clean_count--;
	clear_bit(CB_RESIDENT, &cb->cb_flags);
	return cb;
}


static int cache_access(struct cache_manager *cm, int lba, int op, int fetch)
{
	struct cache_block *cb;		
	dprintf("lba=%d, op=%d CLEN=%d MLEN=%d\n",lba,op,CLEN,MLEN);
	/*cache hit*/
	if ( (cb = cm_search_hash(cm, lba)) != NULL) {
		dprintf("cache hit lba=%d, op=%d\n",lba,op);
		move_to_mru(cb, MIX);
		set_bit(CB_HOT, &cb->cb_flags);
		if ( op == WRITE ) {
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_write_hits++;
			cm_write_block(cb);
			cm->cm_write_hits++;
		}
		else{
			cm->cm_read_hits ++;
			if(test_bit(CB_DIRTY, &cb->cb_flags))
				cm->cm_dirty_read_hits++;
		}
	}

	else {
		if ((cb = cm_get_free_block(cm)) == NULL)
			cb = cache_replace(cm);

		cb->cb_blkno = lba;
		cm_insert_hash(cm, cb);
		if ( op == READ ) {
			move_to_mru(cb, CCL);
			cm_fetch_block(cb, lba);
		}
		else {
			if (fetch)
				cm_fetch_block(cb, lba);
			cm_write_block(cb);
			move_to_mru(cb, MIX);
		}

	}

	return 0;
}


static int cache_read(struct cache_manager *cm, int lba)
{
	cache_access(cm, lba, READ, 1);
	return 0;
}
static int cache_write(struct cache_manager *cm, int lba, int fetch)
{
	cache_access(cm, lba, WRITE, fetch);
	return 0;
}


struct cm_personality ccflru_personality = 
{
	.policy_name = "ccflru",
	.cache_read = cache_read,
	.cache_write = cache_write,
};

int ccflru_register(cache_manager *cm) {
	cm_personality_register(cm, &ccflru_personality);
	return 0;
}
